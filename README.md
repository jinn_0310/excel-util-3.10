## 说明
使用poi-3.10封装的ExcelUtil工具
功能：
- [x] 提供`SourceData`对象格式输入，输出excel文件
- [x] 支持表头合并
- [x] 支持多个sheet导出
- [x] 支持单元格宽度及格式自适应
- [x] 支持自定义表体数据单元格合并
- [x] 支持自定义单元格填充色
- [x] 支持自定义冻结单元格（默认冻结表头）
- [x] 支持自定义单元格内容水平位置（水平居中、水平居左、水平居右），默认水平居中
- [x] 支持自定义单元格内容追至位置（垂直居中、垂直居左、垂直居右），默认垂直居中
- [x] 支持自定义单元格字体、字体大小、是否换行
- [x] 支持Excel2007+版本（Excel2007Util）
- [x] 支持设置自定义格式
- [x] 支持设置对齐缩进
## 运行环境：
1. ≥JDK7
2. Apache POI 3.10
2. pom.xml引入`src/main/lib`中的依赖

## Excel工具（高级版）
1. 工具类：`nc.impl.saas.file.exceladvanced.ExcelAdvancedUtil`
2. 特性如下：
    1. 支持一些自定义单元格配置
    - [x] 数据类型
    - [x] 列宽、行高
    - [x] 是否以此单元格设置冻结单元格
    - [x] 单元格填充色
    - [x] 单元格垂直位置
    - [x] 单元格水平位置
    - [x] 单元格是否支持换行
    - [x] 字体名称、字体大小、是否加粗
    - [x] 自定义格式
    - [x] 对齐缩进量
    - [x] 合并行、合并列（使用colspan、rowspan）
    - [ ] 合并行、合并列（使用mergeKey）
3. 使用示例1：
```java
public class Test {
    public static void main(String[] args) throws Exception {
        // 1. 获取输入源
        List<SourceSheet> sourceSheetList = getTestData();
        // 2. 设置输出目标
        String path = "E:\\xyyuanj\\Downloads\\Excel-Advance-" + System.currentTimeMillis() + ".xlsx";
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file);
        // 3. 调用Excel导出方法
        ExcelAdvancedUtil.buildExcel(sourceSheetList, file.getName(), fos);
        fos.close();
    }
    private static List<SourceSheet> getTestData() {
        List<SourceSheet> sourceSheetList = new ArrayList<>();

        SourceSheet sourceSheet = new SourceSheet("测试");
        sourceSheetList.add(sourceSheet);

        SourceTable sourceTable = sourceSheet.createSourceTable();

        /*
         * 1. 表头配置（树结构）
         * 注意：
         * （1）dataProp 为该列的唯一字段标识名称，用以表体数据自动定位所在列位置
         * （2）表头默认颜色为 Color.GREY_25_PERCENT
         * （3）存在多个table时，可以在表头设置【合并列数】来手动调整table整体宽度
         */
        SourceHeaderCell title1 = sourceTable.createSourceHeaderCell("一级标题", Color.SKY_BLUE);

        title1.createChildren("field-1", "重点项目描述")
                // 设置合并列数
                .setColspan(3)
                // 设置列宽为15字符
                .setWidth(15);
        title1
                .addChildren("field-2", "权重(%)")
                .addChildren("field-3", "上半年考核指标说明")
                .addChildren("field-4", "下半年考核指标说明")
                .addChildren("field-5", "指标类型")
                .addChildren("field-6", "权重(%)");

        /*
         * 2. 表体配置
         * 注意：
         * （1）表体必须依赖表头配置才能成功写入Excel
         * （2）表体中只有【合并行数】配置有效，【合并列数】会自动适应表头中的配置
         * （3）如果单元格数据中有换行符，需要显示声明换行配置才能达到实际的换行效果
         */
        final SourceRow sourceRow = sourceTable.createSourceBodyRow();

        // 以此单元格进行固定行列
        sourceRow
                .addSourceCell("field-1", "行政基础管理工作规范化、标准化建设?", Logic.YES)
                .addSourceCell("field-2", 30, DataType.CELL_TYPE_NUMERIC)
                .addSourceCell("field-3", "协助重新梳理岗位职责、审批流程，建立完善的工作台账。")
                .addSourceCell("field-4", "协助按照内控的要求，做好行政基础管理工作。")
                .addSourceCell("field-5", "定量")
                .addSourceCell("field-6", 100, DataType.CELL_TYPE_NUMERIC);


        SourceRow sourceRow2 = sourceTable.createSourceBodyRow();
        sourceRow2.createSourceCell("field-1", "行政基础管理工作规范化、标准化建设?")
                // 设置单元格行高35
                .setHeight(35)
                // 设置单元格合并行数
                .setRowspan(2)
                // 设置单元格自动换行
                .setWrapText(Logic.YES);
        sourceRow2
                .addSourceCell("field-2", 30, DataType.CELL_TYPE_NUMERIC)
                .addSourceCell("field-3", "协助重新梳理岗位职责、审批流程，建立完善的工作台账。")
                .addSourceCell("field-4", "协助按照内控的要求，做好行政基础管理工作。")
                .addSourceCell("field-5", "定量")
                .addSourceCell("field-6", 100, DataType.CELL_TYPE_NUMERIC);


        SourceRow sourceRow3 = sourceTable.createSourceBodyRow();
        sourceRow3
                .addSourceCell("field-1", "行政基础管理工作规范化、标准化建设?")
                .addSourceCell("field-2", 30, DataType.CELL_TYPE_NUMERIC)
                .addSourceCell("field-3", "协助重新梳理岗位职责、审批流程，建立完善的工作台账。")
                .addSourceCell("field-4", "协助按照内控的要求，做好行政基础管理工作。")
                .addSourceCell("field-5", "定量")
                .addSourceCell("field-6", 100, DataType.CELL_TYPE_NUMERIC);

        /*
         * 3. 附加项配置
         */
        // 添加空行
        sourceTable.addSourceAdditionalRow();
        sourceTable.addSourceAdditionalRow(
                new SourceCell("员工自评").setColspan(4).setBold(Logic.YES)
        );
        sourceTable.addSourceAdditionalRow(
                new SourceCell("优点").setBold(Logic.YES),
                new SourceCell("这是优点的内容").setColspan(3).setAlign(Align.LEFT)
        );
        sourceTable.addSourceAdditionalRow(
                new SourceCell("缺点").setBold(Logic.YES),
                new SourceCell("这是缺点的内容").setColspan(3).setAlign(Align.LEFT)
        );

        return sourceSheetList;
    }

}
```
3.1、 示例1图片：
![ExcelAdcancedUtil-示例.png](https://s2.loli.net/2022/01/19/7CAbcMxZeyvlW12.png)

---

4、 使用示例2：
```java
import nc.impl.saas.file.exceladvanced.ExcelAdvancedUtil;
import nc.impl.saas.file.exceladvanced.consts.Logic;
import nc.impl.saas.file.exceladvanced.dto.SourceRow;
import nc.impl.saas.file.exceladvanced.dto.SourceSheet;
import nc.impl.saas.file.exceladvanced.dto.SourceTable;

import java.io.FileOutputStream;

public class ExcelAdvanceUtilTest2 {
    public static void main(String[] args) throws Exception {
        SourceSheet sourceSheet = new SourceSheet("sheet1");

        SourceTable sourceTable = sourceSheet.createSourceTable();

        sourceTable
                .addSourceHeaderCell("title_1", "姓名")
                .addSourceHeaderCell("title_2", "性别")
                .addSourceHeaderCell("title_3", "年龄")
                .addSourceHeaderCell("title_4", "联系方式");


        sourceTable.addSourceBodyRow(
                new SourceRow()
                        .addSourceCell("title_1", "张三", Logic.YES)
                        .addSourceCell("title_2", "男")
                        .addSourceCell("title_3", "26", DataType.CELL_TYPE_NUMERIC)
                        .addSourceCell("title_4", "13066669999", 30)
        );
        sourceTable.addSourceBodyRow(
                new SourceRow()
                        .addSourceCell("title_1", "李四")
                        .addSourceCell("title_2", "女")
                        .addSourceCell("title_3", "30", DataType.CELL_TYPE_NUMERIC)
                        .addSourceCell("title_4", "13700001111")
        );

        String path = "E:\\users\\Downloads\\导出工具测试.xlsx";

        File file = new File(path);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try (FileOutputStream fos = new FileOutputStream(path)) {
            ExcelAdvancedUtil.buildExcel(sourceSheet, fos);
        }


    }
}
```


4.1. 示例2截图

![QQ截图20220425105243.png](https://s2.loli.net/2022/04/25/9hZfcLAGFau4lyr.png)