> 该版本已不再更新，最新工具为 `nc.impl.saas.file.exceladvanced.ExcelAdvancedUtil`

## Excel工具（第二版）
1. 工具类：`nc.impl.saas.file.Excel2007Util`
1. dataObjList: 数据体，每行数据，按顺序写入Excel
    1) value：单元格的值，必要
    2) dataType: 单元格类型，非必要，默认为1（字符串）
    3) splitFreeze: 是否以此单元格设置"冻结窗格"，非必要，一个sheet只允许一个单元格设置该属性
    4) key: 单元格标识，非必要，如果不为空，则会检查是否需要合并相邻的单元格（相邻单元格，检查到相同key时会进行合并，且只取第一个单元格的值）
    5) dataProp: 非必要，数据字段名
2. footObjList: 同 `dataObjList`
3. headerList: 表头, 每列层级关系用树形结构
    1) title: 单元格值，必要
    2) dataType: 单元格的值，必要
    3) children: 子标题数组
    4) titleKey: 单元格标识，非必要
    5) dataProp: 单元格类型，非必要，默认为1（字符串）（为第一版的属性）
4. sheetName: sheet名称
```json
[
  {
    "dataObjList": [
      [
        {
          "dataProp": "orgDeptName",
          "dataType": 0,
          "value": "集团本部象屿集团总裁室"
        },
        {
          "dataProp": "SCORE_0001AA100000001879L4_01",
          "dataType": 0,
          "value": 11,
          "splitFreeze": "Y"
        },
        {
          "dataProp": "SCORE_0001AA100000001879L4_02",
          "dataType": 0,
          "value": 22
        },
        {
          "dataProp": "0001AA100000001879L4_totalScore",
          "dataType": 0,
          "value": 33
        },
        {
          "dataProp": "SCORE_0001AA100000001879L7_01",
          "dataType": 0,
          "value": 11
        },
        {
          "dataProp": "SCORE_0001AA100000001879L7_02",
          "dataType": 0,
          "value": 22
        },
        {
          "dataProp": "0001AA100000001879L7_totalScore",
          "dataType": 0,
          "value": 33
        },
        {
          "dataProp": "yearScore",
          "dataType": 0,
          "value": 66
        },
        {
          "dataProp": "extendscore_1",
          "dataType": 0,
          "value": 3
        },
        {
          "dataProp": "remark_1",
          "dataType": 0,
          "value": "3"
        },
        {
          "dataProp": "extendscore_2",
          "dataType": 0,
          "value": 0
        },
        {
          "dataProp": "remark_2",
          "dataType": 0
        },
        {
          "dataProp": "extendscore_3",
          "dataType": 0,
          "value": 0
        },
        {
          "dataProp": "remark_3",
          "dataType": 0
        },
        {
          "dataProp": "yearTotalScore",
          "dataType": 0,
          "value": 69
        }
      ]
    ],
    "footObjList": [],
    "headerList": [
      {
        "children": [
          {
            "children": [],
            "dataProp": "orgDeptName",
            "dataType": 0,
            "title": "被考核公司/部门",
            "titleKey": "被考核公司/部门"
          },
          {
            "children": [
              {
                "children": [],
                "dataProp": "SCORE_0001AA100000001879L4_01",
                "dataType": 0,
                "title": "1.1",
                "titleKey": "0001AA100000001879L4"
              },
              {
                "children": [],
                "dataProp": "SCORE_0001AA100000001879L4_02",
                "dataType": 0,
                "title": "1.2",
                "titleKey": "0001AA100000001879L4"
              },
              {
                "children": [],
                "dataProp": "0001AA100000001879L4_totalScore",
                "dataType": 0,
                "title": "得分",
                "titleKey": "得分"
              }
            ],
            "dataProp": "",
            "dataType": 0,
            "title": "组织--e1",
            "titleKey": "0001AA100000001879L4"
          },
          {
            "children": [
              {
                "children": [],
                "dataProp": "SCORE_0001AA100000001879L7_01",
                "dataType": 0,
                "title": "1.1",
                "titleKey": "0001AA100000001879L7"
              },
              {
                "children": [],
                "dataProp": "SCORE_0001AA100000001879L7_02",
                "dataType": 0,
                "title": "1.2",
                "titleKey": "0001AA100000001879L7"
              },
              {
                "children": [],
                "dataProp": "0001AA100000001879L7_totalScore",
                "dataType": 0,
                "title": "得分",
                "titleKey": "得分"
              }
            ],
            "dataProp": "",
            "dataType": 0,
            "title": "组织--e2",
            "titleKey": "0001AA100000001879L7"
          },
          {
            "children": [],
            "dataProp": "yearScore",
            "dataType": 0,
            "title": "合计得分",
            "titleKey": "合计得分"
          },
          {
            "children": [
              {
                "children": [],
                "dataProp": "extendscore_1",
                "dataType": 0,
                "title": "调整分",
                "titleKey": "调整分"
              },
              {
                "children": [],
                "dataProp": "remark_1",
                "dataType": 0,
                "title": "调整说明",
                "titleKey": "调整说明"
              }
            ],
            "dataProp": "",
            "dataType": 0,
            "title": "马萧",
            "titleKey": "马萧"
          },
          {
            "children": [
              {
                "children": [],
                "dataProp": "extendscore_2",
                "dataType": 0,
                "title": "调整分",
                "titleKey": "调整分"
              },
              {
                "children": [],
                "dataProp": "remark_2",
                "dataType": 0,
                "title": "调整说明",
                "titleKey": "调整说明"
              }
            ],
            "dataProp": "",
            "dataType": 0,
            "title": "刘林鑫",
            "titleKey": "刘林鑫"
          },
          {
            "children": [
              {
                "children": [],
                "dataProp": "extendscore_3",
                "dataType": 0,
                "title": "调整分",
                "titleKey": "调整分"
              },
              {
                "children": [],
                "dataProp": "remark_3",
                "dataType": 0,
                "title": "调整说明",
                "titleKey": "调整说明"
              }
            ],
            "dataProp": "",
            "dataType": 0,
            "title": "陆博茜",
            "titleKey": "陆博茜"
          },
          {
            "children": [],
            "dataProp": "yearTotalScore",
            "dataType": 0,
            "title": "最终得分",
            "titleKey": "最终得分"
          }
        ],
        "dataProp": "",
        "dataType": 0,
        "title": "组织--e",
        "titleKey": "组织--e"
      }
    ],
    "sheetName": "汇总得分"
  }
]
```