package nc.itf.saas.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 解析异常日志
 */
public class ExceptionParseUtil {

    public static String parse(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(stringWriter);
            e.printStackTrace(writer);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        return stringWriter.getBuffer().toString();
    }
}
