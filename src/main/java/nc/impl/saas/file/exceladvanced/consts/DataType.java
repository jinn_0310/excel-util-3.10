package nc.impl.saas.file.exceladvanced.consts;

/**
 * 单元格数据类型
 *
 * @author xyyuanj
 */

public enum DataType {
    /**
     * 单元格数据类型
     */
    CELL_TYPE_NUMERIC(0, "数值型"),
    CELL_TYPE_STRING(1, "文本型"),
    CELL_TYPE_FORMULA(2, "公式型"),
    CELL_TYPE_BLANK(3, "空值"),
    CELL_TYPE_BOOLEAN(4, "布尔型"),
    CELL_TYPE_ERROR(5, "错误");

    /**
     * 索引值；同 XSSFCell.CELL_TYPE_*
     */
    private int idx;
    private String zhDesc;

    DataType(int idx, String zhDesc) {
        this.idx = idx;
        this.zhDesc = zhDesc;
    }

    public int getIdx() {
        return idx;
    }

    public String getZhDesc() {
        return zhDesc;
    }
}
