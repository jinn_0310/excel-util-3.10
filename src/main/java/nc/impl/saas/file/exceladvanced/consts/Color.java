package nc.impl.saas.file.exceladvanced.consts;

import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 * 颜色参照表
 *
 * @author xyyuanj
 */

public enum Color {
    /**
     * 颜色参照表
     */
    BLACK(8, new XSSFColor(new java.awt.Color(0, 0, 0))),
    WHITE(9, new XSSFColor(new java.awt.Color(255, 255, 255))),
    RED(10, new XSSFColor(new java.awt.Color(255, 0, 0))),
    BRIGHT_GREEN(11, new XSSFColor(new java.awt.Color(0, 255, 0))),
    BLUE(12, new XSSFColor(new java.awt.Color(0, 0, 255))),
    YELLOW(13, new XSSFColor(new java.awt.Color(255, 255, 0))),
    PINK(14, new XSSFColor(new java.awt.Color(255, 0, 255))),
    TURQUOISE(15, new XSSFColor(new java.awt.Color(0, 255, 255))),
    DARK_RED(16, new XSSFColor(new java.awt.Color(128, 0, 0))),
    GREEN(17, new XSSFColor(new java.awt.Color(0, 128, 0))),
    DARK_BLUE(18, new XSSFColor(new java.awt.Color(0, 0, 128))),
    DARK_YELLOW(19, new XSSFColor(new java.awt.Color(128, 128, 0))),
    VIOLET(20, new XSSFColor(new java.awt.Color(128, 0, 128))),
    TEAL(21, new XSSFColor(new java.awt.Color(0, 128, 128))),
    GREY_25_PERCENT(22, new XSSFColor(new java.awt.Color(192, 192, 192))),
    GREY_50_PERCENT(23, new XSSFColor(new java.awt.Color(128, 128, 128))),
    CORNFLOWER_BLUE(24, new XSSFColor(new java.awt.Color(153, 153, 255))),
    MAROON(25, new XSSFColor(new java.awt.Color(153, 51, 102))),
    LEMON_CHIFFON(26, new XSSFColor(new java.awt.Color(255, 255, 204))),
    ORCHID(28, new XSSFColor(new java.awt.Color(102, 0, 102))),
    CORAL(29, new XSSFColor(new java.awt.Color(255, 128, 128))),
    ROYAL_BLUE(30, new XSSFColor(new java.awt.Color(0, 102, 204))),
    LIGHT_CORNFLOWER_BLUE(31, new XSSFColor(new java.awt.Color(204, 204, 255))),
    SKY_BLUE(40, new XSSFColor(new java.awt.Color(0, 204, 255))),
    LIGHT_TURQUOISE(41, new XSSFColor(new java.awt.Color(204, 255, 255))),
    LIGHT_GREEN(42, new XSSFColor(new java.awt.Color(204, 255, 204))),
    LIGHT_YELLOW(43, new XSSFColor(new java.awt.Color(255, 255, 153))),
    PALE_BLUE(44, new XSSFColor(new java.awt.Color(153, 204, 255))),
    ROSE(45, new XSSFColor(new java.awt.Color(255, 153, 204))),
    LAVENDER(46, new XSSFColor(new java.awt.Color(204, 153, 255))),
    TAN(47, new XSSFColor(new java.awt.Color(255, 204, 153))),
    LIGHT_BLUE(48, new XSSFColor(new java.awt.Color(51, 102, 255))),
    AQUA(49, new XSSFColor(new java.awt.Color(51, 204, 204))),
    LIME(50, new XSSFColor(new java.awt.Color(153, 204, 0))),
    GOLD(51, new XSSFColor(new java.awt.Color(255, 204, 0))),
    LIGHT_ORANGE(52, new XSSFColor(new java.awt.Color(255, 153, 0))),
    ORANGE(53, new XSSFColor(new java.awt.Color(255, 102, 0))),
    BLUE_GREY(54, new XSSFColor(new java.awt.Color(102, 102, 153))),
    GREY_40_PERCENT(55, new XSSFColor(new java.awt.Color(150, 150, 150))),
    DARK_TEAL(56, new XSSFColor(new java.awt.Color(0, 51, 102))),
    SEA_GREEN(57, new XSSFColor(new java.awt.Color(51, 153, 102))),
    DARK_GREEN(58, new XSSFColor(new java.awt.Color(0, 51, 0))),
    OLIVE_GREEN(59, new XSSFColor(new java.awt.Color(51, 51, 0))),
    BROWN(60, new XSSFColor(new java.awt.Color(153, 51, 0))),
    PLUM(61, new XSSFColor(new java.awt.Color(153, 51, 102))),
    INDIGO(62, new XSSFColor(new java.awt.Color(51, 51, 153))),
    GREY_80_PERCENT(63, new XSSFColor(new java.awt.Color(51, 51, 51))),
    AUTOMATIC(64, new XSSFColor(new java.awt.Color(255, 255, 255)));

    public short idx;
    private XSSFColor xssfColor;

    Color(int idx, XSSFColor xssfColor) {
        this.idx = (short) idx;
        this.xssfColor = xssfColor;
    }

    public static XSSFColor getXssfColorByIdx(Short idx) {
        if (idx != null && idx > 0) {
            for (Color color : Color.values()) {
                if (color.getIdx() == idx) {
                    return color.getXssfColor();
                }
            }
        }
        return null;
    }

    public short getIdx() {
        return idx;
    }

    public XSSFColor getXssfColor() {
        return xssfColor;
    }
}
