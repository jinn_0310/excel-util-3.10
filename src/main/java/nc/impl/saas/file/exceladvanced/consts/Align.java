package nc.impl.saas.file.exceladvanced.consts;

/**
 * 水平位置
 *
 * @author xyyuanj
 */
public enum Align {
    /**
     * 水平位置
     */
    GENERAL((short) 0, "默认"),
    LEFT((short) 1, "水平居左"),
    CENTER((short) 2, "水平居中"),
    RIGHT((short) 3, "水平居右"),
    FILL((short) 4, "水平填充"),
    JUSTIFY((short) 5, "水平对齐"),
    CENTER_SELECTION((short) 6, "水平居中_选择");

    private short idx;
    private String zhDesc;

    Align(short idx, String zhDesc) {
        this.idx = idx;
        this.zhDesc = zhDesc;
    }

    public short getIdx() {
        return idx;
    }

    public String getZhDesc() {
        return zhDesc;
    }
}
