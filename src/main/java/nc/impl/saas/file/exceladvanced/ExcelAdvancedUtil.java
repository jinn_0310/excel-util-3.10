package nc.impl.saas.file.exceladvanced;

import nc.impl.saas.file.exceladvanced.consts.Const;
import nc.impl.saas.file.exceladvanced.dto.*;
import nc.impl.saas.file.exceladvanced.method.LoadDataPropColIndex;
import nc.impl.saas.file.exceladvanced.tool.CacheTool;
import nc.impl.saas.file.exceladvanced.tool.PrintLog;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

/**
 * Excel工具（高级版）
 *
 * @author xyyuanj
 * @since 2022.01.18 特性如下：
 * 1.支持一些自定义单元格配置
 * （1）数据类型
 * （2）宽度
 * （3）高度
 * （4）是否以此单元格设置冻结单元格
 * （5）单元格填充色
 * （6）单元格垂直位置
 * （7）单元格水平位置
 * （8）单元格是否支持换行
 * （9）字体名称、字体大小、是否加粗
 * （10）自定义格式
 * （11）对齐缩进量
 * （12）合并行数
 * （13）合并列数
 */
public class ExcelAdvancedUtil {
    public static void buildExcel(SourceSheet sourceSheet, OutputStream outputStream) throws Exception {
        buildExcel(Collections.singletonList(sourceSheet), "default.xlsx", outputStream);
    }

    /**
     * 组建Excel
     *
     * @param sourceSheetList 源数据
     * @param fileName        Excel文件名（包括后缀）
     * @param outputStream    输出流
     */
    public static void buildExcel(List<SourceSheet> sourceSheetList, String fileName, OutputStream outputStream) throws Exception {
        InputStream inputStream = null;
        try {
            inputStream = buildExcel(sourceSheetList, fileName);
            writeOutputStream(inputStream, outputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * 组建Excel
     *
     * @param sourceSheetList 源数据
     * @param fileName        Excel文件名（包括后缀）
     * @return Excel的输入流
     */
    public static InputStream buildExcel(List<SourceSheet> sourceSheetList, String fileName) throws Exception {
        Workbook workbook = getWorkBook(fileName);
        try {
            for (SourceSheet sourceSheet : sourceSheetList) {
                buildSheet(workbook, sourceSheet);
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } finally {
            CacheTool.clear(workbook);
        }
    }

    private static void writeOutputStream(InputStream inputStream, OutputStream outputStream) throws IOException {
        int length;
        byte[] bytes = new byte[1024];
        while ((length = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, length);
        }
    }

    private static void buildSheet(Workbook workbook, SourceSheet sourceSheet) {
        Sheet sheet = workbook.createSheet(sourceSheet.getSheetName());

        List<SourceTable> sourceTableList = sourceSheet.getSourceTableList();

        // 当前Sheet中数据行数
        int curRowNum = -1;
        for (SourceTable sourceTable : sourceTableList) {
            curRowNum = buildTableInSheet(workbook, sheet, sourceTable, curRowNum);
        }

    }

    /**
     * 在Sheet写入表数据
     *
     * @param curRowIndex 当前行下标
     * @return 当前Sheet中数据行数
     */
    private static int buildTableInSheet(Workbook workbook, final Sheet sheet, final SourceTable sourceTable, int curRowIndex) {
        // 前面空行
        curRowIndex += sourceTable.getPrependEmptyRowNum();
        // dataProp对应的列下标
        final Map<String, List<Integer>> dataProp2ColIndexListMap = new HashMap<>();

        List<List<SourceCell>> headerList = new ArrayList<>();
        // 表头层级数
        PrintLog.println("开始获取表头层级数 >>>>>>>>");
        int maxLevel = getMaxLevel(sourceTable.getSourceHeaderCellList());
        PrintLog.println("表头层级数={}", maxLevel);
        // 重载表头数据结构
        PrintLog.println("开始重载表头数据结构 >>>>>>>>");
        recursionLoadHeaderList(sourceTable.getSourceHeaderCellList(), headerList, maxLevel, 0, 0);
        PrintLog.println("重载表头数据结构结束");

        // 写入表头部
        PrintLog.println("开始写入表头部 >>>>>>>>");
        curRowIndex = writeRowDataOfTableInSheetCanMergeRowCol(workbook, sheet, headerList, curRowIndex, new LoadDataPropColIndex() {
            @Override
            public void load(SourceHeaderCell sourceHeaderCell, Integer colIndex) {
                // 设置每个字段对应的列下标
                String dataProp = sourceHeaderCell.getDataProp();
                if (dataProp != null && dataProp.length() > 0) {
                    if (!dataProp2ColIndexListMap.containsKey(dataProp)) {
                        dataProp2ColIndexListMap.put(dataProp, new ArrayList<Integer>());
                    }
                    dataProp2ColIndexListMap.get(dataProp).add(colIndex);
                }
                // 重置表体中每个单元格的列合并参数
                int colspan = sourceHeaderCell.getColspan();
                for (SourceRow sourceRow : sourceTable.getSourceBodyList()) {
                    Map<String, SourceCell> sourceCellList = sourceRow.getSourceCellList();
                    if (sourceCellList.containsKey(dataProp)) {
                        SourceCell sourceCell = sourceCellList.get(dataProp);
                        sourceCell.setColspan(colspan);
                    }
                }
            }
        });
        PrintLog.println("写入表头部完成");

        // 写入表体
        PrintLog.println("开始写入表体 >>>>>>>>");
        curRowIndex = writeRowDataOfTableInSheetOnlyMergeRow(workbook, sheet, sourceTable.getSourceBodyList(), curRowIndex, dataProp2ColIndexListMap);
        PrintLog.println("写入表体完成");

        // 写入附加项
        PrintLog.println("开始写入附加项 >>>>>>>>");
        curRowIndex = writeRowDataOfTableInSheetCanMergeRowCol(workbook, sheet, sourceTable.getSourceAdditionalList(), curRowIndex);
        PrintLog.println("写入附加项完成");

        // 后面空行
        curRowIndex += sourceTable.getAppendEmptyRowNum();
        return curRowIndex;
    }

    private static int writeRowDataOfTableInSheetCanMergeRowCol(Workbook workbook, Sheet sheet, List<List<SourceCell>> sourceAdditionalList, int curRowIndex) {
        return writeRowDataOfTableInSheetCanMergeRowCol(workbook, sheet, sourceAdditionalList, curRowIndex, null);
    }

    /**
     * 获取表头的层数
     *
     * @param sourceHeaderCellList 表头数据
     * @return 层级数量
     */
    private static int getMaxLevel(List<SourceHeaderCell> sourceHeaderCellList) {
        for (SourceHeaderCell sourceHeaderCell : sourceHeaderCellList) {
            if (sourceHeaderCell.getChildren() != null && sourceHeaderCell.getChildren().size() > 0) {
                int level = getMaxLevel(sourceHeaderCell.getChildren());
                return 1 + level;
            }
        }
        return 0;
    }

    /**
     * 获取叶子节点数量
     */
    private static int getLeafNum(SourceHeaderCell sourceHeaderCell) {
        if (sourceHeaderCell == null) {
            return 0;
        } else {
            if (sourceHeaderCell.getChildren() != null && sourceHeaderCell.getChildren().size() > 0) {
                int num = 0;
                for (SourceHeaderCell child : sourceHeaderCell.getChildren()) {
                    num += getLeafNum(child);
                }
                return num;
            } else {
                return 1;
            }
        }
    }

    /**
     * 重载表头数据结构
     */
    private static void recursionLoadHeaderList(List<SourceHeaderCell> sourceHeaderCellList, List<List<SourceCell>> headerList, int maxLevel, int level, int colIndex) {
        if (sourceHeaderCellList == null || sourceHeaderCellList.size() == 0) {
            return;
        }
        if (level == headerList.size()) {
            headerList.add(new ArrayList<SourceCell>());
        }
        List<SourceCell> row = headerList.get(level);

        for (int i = 0; i < sourceHeaderCellList.size(); i++) {
            SourceHeaderCell sourceHeaderCell = sourceHeaderCellList.get(i);
            row.add(sourceHeaderCell);

            if (sourceHeaderCell.getChildren() != null && sourceHeaderCell.getChildren().size() > 0) {
                // 叶子节点的列合并数累加
                int leafColspanSum = getLeafColspanSum(sourceHeaderCell);
                if (leafColspanSum > 1) {
                    sourceHeaderCell.setColspan(leafColspanSum);
                }
                recursionLoadHeaderList(sourceHeaderCell.getChildren(), headerList, maxLevel, level + 1, colIndex + i);
            } else {
                // 如果为空，则判断同级是否需要合并
                sourceHeaderCell.setRowspan(maxLevel - level + 1);
            }
        }

    }

    private static int getLeafColspanSum(SourceHeaderCell sourceHeaderCell) {
        if (sourceHeaderCell == null) {
            return 0;
        } else {
            if (sourceHeaderCell.getChildren() != null && sourceHeaderCell.getChildren().size() > 0) {
                int num = 0;
                for (SourceHeaderCell child : sourceHeaderCell.getChildren()) {
                    num += getLeafColspanSum(child);
                }
                return num;
            } else {
                return sourceHeaderCell.getColspan();
            }
        }
    }

    /**
     * 写入数据（允许合并行、合并列）
     *
     * @return 当前Sheet中数据行数
     */
    private static int writeRowDataOfTableInSheetCanMergeRowCol(Workbook workbook, Sheet sheet, List<List<SourceCell>> sourceAdditionalList, int curRowIndex, LoadDataPropColIndex loadDataPropColIndex) {
        for (List<SourceCell> sourceCellList : sourceAdditionalList) {
            curRowIndex++;
            Row row = sheet.getRow(curRowIndex);
            if (row == null) {
                row = sheet.createRow(curRowIndex);
            }
            int colNum = 0;
            int rowHeight = -1;
            for (SourceCell sourceCell : sourceCellList) {
                if (sourceCell.getHeight() > rowHeight) {
                    rowHeight = sourceCell.getHeight();
                }
                Cell cell = getOneNeverMergedCell(sheet, row, colNum);
                colNum = cell.getColumnIndex();

                // 设置列宽
                setSheetColumnWidth(sheet, colNum, sourceCell.getWidth());
                // 如果是头部数据，则需要允许加载字段与列下标的映射
                if (loadDataPropColIndex != null) {
                    if (sourceCell instanceof SourceHeaderCell) {
                        SourceHeaderCell sourceHeaderCell = (SourceHeaderCell) sourceCell;
                        loadDataPropColIndex.load(sourceHeaderCell, colNum);
                    }
                }
                cell = row.createCell(colNum);
                setCellValue(cell, sourceCell.getValue());
                cell.setCellType(sourceCell.getDataType().getIdx());
                CellStyle cellStyle = createCellStyle(workbook, sourceCell);
                cell.setCellStyle(cellStyle);

                if (sourceCell.getRowspan() > 1 || sourceCell.getColspan() > 1) {
                    // 添加合并（行合并、列合并）
                    int lastRow = curRowIndex + sourceCell.getRowspan() - 1;
                    int lastCol = colNum + sourceCell.getColspan() - 1;
                    CellRangeAddress cra = new CellRangeAddress(curRowIndex, lastRow, colNum, lastCol);
                    sheet.addMergedRegion(cra);
                    // 设置合并的单元格的样式
                    for (int i = curRowIndex; i <= lastRow; i++) {
                        Row mergeRow = sheet.getRow(i);
                        if (mergeRow == null) {
                            mergeRow = sheet.createRow(i);
                        }
                        for (int j = colNum; j <= lastCol; j++) {
                            Cell mergeCell = mergeRow.getCell(j);
                            if (mergeCell == null) {
                                mergeCell = mergeRow.createCell(j);
                            }
                            mergeCell.setCellStyle(cellStyle);
                        }
                    }
                }
                colNum++;
            }
            // 设置行高
            if (rowHeight > 0) {
                row.setHeight((short) (Const.ROW_UNIT * rowHeight));
            }
        }
        return curRowIndex;
    }

    private static void setSheetColumnWidth(Sheet sheet, int colIndex, Integer width) {
        int newColWidth = Const.MIN_COL_WIDTH;
        int currentColWidth = sheet.getColumnWidth(colIndex);
        if (width != null && width > Const.MIN_COL_WIDTH) {
            if (currentColWidth < width * Const.COL_UNIT) {
                newColWidth = width * Const.COL_UNIT;
            }
        } else {
            newColWidth = currentColWidth;
        }
        if (currentColWidth < newColWidth) {
            sheet.setColumnWidth(colIndex, newColWidth);
        }
    }

    private static void setCellValue(Cell cell, Object value) {
        if (value != null) {
            if (value instanceof Double) {
                cell.setCellValue(Double.parseDouble(String.valueOf(value)));
            } else if (value instanceof Integer) {
                cell.setCellValue(Integer.parseInt(String.valueOf(value)));
            } else if (value instanceof Short) {
                cell.setCellValue(Short.parseShort(String.valueOf(value)));
            } else {
                cell.setCellValue(String.valueOf(value));
            }
        } else {
            cell.setCellValue("");
        }
    }

    /**
     * 从当前列下标开始，获取一个当前行中未被合并的单元格
     *
     * @param sheet  当前Sheet
     * @param row    当前行
     * @param colNum 当前下标
     * @return 当前行中未被合并的单元格
     */
    private static Cell getOneNeverMergedCell(Sheet sheet, Row row, int colNum) {
        Cell cell = null;
        if (isAlreadyMergeCell(sheet, row.getRowNum(), colNum)) {
            cell = getOneNeverMergedCell(sheet, row, colNum + 1);
        } else {
            cell = row.createCell(colNum);
            return cell;
        }
        return cell;
    }

    /**
     * 判断该单元格是否被合并
     *
     * @param sheet    当前Sheet
     * @param rowIndex 当前行下标
     * @param colIndex 当前列下标
     * @return 是否被合并
     */
    private static boolean isAlreadyMergeCell(Sheet sheet, int rowIndex, int colIndex) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (rowIndex >= firstRow && rowIndex <= lastRow) {
                if (colIndex >= firstColumn && colIndex <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 写入表体（仅合并行）
     *
     * @return 当前Sheet中数据行数
     */
    private static int writeRowDataOfTableInSheetOnlyMergeRow(Workbook workbook, Sheet sheet, List<SourceRow> sourceBodyList, int curRowIndex, Map<String, List<Integer>> dataProp2ColIndexListMap) {
        for (SourceRow sourceRow : sourceBodyList) {
            curRowIndex++;
            if (!sourceRow.getEmpty()) {
                // 行高
                int rowHeight = -1;
                Row row = sheet.getRow(curRowIndex);
                if (row == null) {
                    row = sheet.createRow(curRowIndex);
                }
                Map<String, SourceCell> sourceCellList = sourceRow.getSourceCellList();
                for (Map.Entry<String, SourceCell> entry : sourceCellList.entrySet()) {
                    String dataProp = entry.getKey();
                    SourceCell sourceCell = entry.getValue();
                    if (sourceCell.getHeight() > rowHeight) {
                        rowHeight = sourceCell.getHeight();
                    }
                    List<Integer> colList = dataProp2ColIndexListMap.get(dataProp);
                    if (colList != null && colList.size() > 0) {
                        Integer colIndex = colList.get(0);
                        Cell cell = getOneNeverMergedCell(sheet, row, colIndex);
                        if (cell != null) {
                            // 设置列宽
                            setSheetColumnWidth(sheet, colIndex, sourceCell.getWidth());
                            setCellValue(cell, sourceCell.getValue());
                            cell.setCellType(sourceCell.getDataType().getIdx());
                            CellStyle cellStyle = createCellStyle(workbook, sourceCell);
                            cell.setCellStyle(cellStyle);

                            if (sourceCell.getRowspan() > 1 || sourceCell.getColspan() > 1) {
                                // 添加合并（行合并）
                                int lastRow = curRowIndex + sourceCell.getRowspan() - 1;
                                int lastCol = colIndex + sourceCell.getColspan() - 1;
                                CellRangeAddress cra = new CellRangeAddress(curRowIndex, lastRow, colIndex, lastCol);
                                sheet.addMergedRegion(cra);
                                // 设置合并的单元格的样式
                                setCellStyleForMergeRowForBody(sheet, curRowIndex, colIndex, cellStyle, lastRow, lastCol);
                            }

                            // 设置单元格，冻结窗格
                            if (sourceCell.getSplitFreeze().isTrue()) {
                                int colSplit = colIndex;
                                int rowSplit = curRowIndex;
                                int leftMostColumn = colIndex;
                                int topRow = curRowIndex;
                                sheet.createFreezePane(colSplit, rowSplit, leftMostColumn, topRow);
                            }
                        }
                    }
                }
                // 设置行高
                if (rowHeight > 0) {
                    row.setHeight((short) (Const.ROW_UNIT * rowHeight));
                }
            }
        }
        return curRowIndex;
    }

    private static void setCellStyleForMergeRowForBody(Sheet sheet, int curRowIndex, Integer colIndex, CellStyle cellStyle, int lastRow, int lastCol) {
        for (int i = curRowIndex; i <= lastRow; i++) {
            Row mergeRow = sheet.getRow(i);
            if (mergeRow == null) {
                mergeRow = sheet.createRow(i);
            }
            for (int j = colIndex; j <= lastCol; j++) {
                Cell mergeCell = mergeRow.getCell(j);
                if (mergeCell == null) {
                    mergeCell = mergeRow.createCell(j);
                }
                mergeCell.setCellStyle(cellStyle);
            }

        }
    }

    private static Workbook getWorkBook(String fileName) {
        if (fileName.endsWith(Const.SUFFIX_XLS)) {
            return new HSSFWorkbook();
        } else if (fileName.endsWith(Const.SUFFIX_XLSX)) {
            return new XSSFWorkbook();
        } else {
            throw new IllegalArgumentException("文件后缀不是.xls或.xlsx");
        }
    }

    private static CellStyle createCellStyle(Workbook workbook) {
        SourceCell sourceCell = new SourceCell();
        return createCellStyle(workbook, sourceCell);
    }

    private static CellStyle createCellStyle(Workbook workbook, SourceCell sourceCell) {
        return CacheTool.getCellStyle(workbook, sourceCell);
    }


}
