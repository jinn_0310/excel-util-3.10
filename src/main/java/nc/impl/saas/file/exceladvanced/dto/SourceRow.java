package nc.impl.saas.file.exceladvanced.dto;

import nc.impl.saas.file.exceladvanced.consts.DataType;
import nc.impl.saas.file.exceladvanced.consts.Logic;

import java.util.HashMap;
import java.util.Map;

/**
 * 输入源：行配置
 *
 * @author xyyuanj
 */
public class SourceRow {
    public static final SourceRow EMPTY_SOURCE_ROW = new SourceRow(true);

    /**
     * 一行中单元格
     * key: dataProp, value: 单元格配置
     */
    private Map<String, SourceCell> sourceCellList;
    /**
     * 是否为空行
     */
    private Boolean empty;

    public SourceRow() {
        this.empty = false;
        this.sourceCellList = new HashMap<>();
    }

    public SourceRow(Boolean empty) {
        this.empty = empty;
    }

    public Map<String, SourceCell> getSourceCellList() {
        return sourceCellList;
    }

    public void setSourceCellList(Map<String, SourceCell> sourceCellList) {
        this.sourceCellList = sourceCellList;
    }

    public Boolean getEmpty() {
        return empty;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }


    public SourceRow addSourceCell(String dataProp, SourceCell sourceCell) {
        this.sourceCellList.put(dataProp, sourceCell);
        return this;
    }

    public SourceRow addSourceCell(String dataProp, Object value) {
        return addSourceCell(dataProp, value, null, null, null);
    }

    public SourceRow addSourceCell(String dataProp, Object value, Logic splitFreeze) {
        return addSourceCell(dataProp, value, null, null, splitFreeze);
    }

    public SourceRow addSourceCell(String dataProp, Object value, DataType dataType) {
        return addSourceCell(dataProp, value, dataType, null, null);
    }

    public SourceRow addSourceCell(String dataProp, Object value, Integer width, Integer height) {
        return addSourceCell(dataProp, value, null, width, null);
    }

    public SourceRow addSourceCell(String dataProp, Object value, DataType dataType, Integer width, Logic splitFreeze) {
        if (!this.sourceCellList.containsKey(dataProp)) {
            SourceCell sourceCell = new SourceCell();
            sourceCell.setValue(value);
            if (dataType != null) {
                sourceCell.setDataType(dataType);
            }
            if (width != null && width > 0) {
                sourceCell.setWidth(width);
            }
            if (splitFreeze != null) {
                sourceCell.setSplitFreeze(splitFreeze);
            }
            this.sourceCellList.put(dataProp, sourceCell);
        }
        return this;
    }

    public SourceRow addSourceCell(String dataProp, Object value, DataType dataType, Integer width, Logic splitFreeze, Integer height) {
        if (!this.sourceCellList.containsKey(dataProp)) {
            SourceCell sourceCell = new SourceCell();
            sourceCell.setValue(value);
            if (dataType != null) {
                sourceCell.setDataType(dataType);
            }
            if (width != null && width > 0) {
                sourceCell.setWidth(width);
            }
            if (splitFreeze != null) {
                sourceCell.setSplitFreeze(splitFreeze);
            }
            if (height != null && height > 0) {
                sourceCell.setHeight(height);
            }
            this.sourceCellList.put(dataProp, sourceCell);
        }
        return this;
    }

    public SourceRow addSourceCell(String dataProp, Object value, Integer width) {
        return addSourceCell(dataProp, value, null, width, null);
    }

    public SourceCell createSourceCell(String dataProp, Object value) {
        SourceCell sourceCell = new SourceCell(value);
        this.sourceCellList.put(dataProp, sourceCell);
        return sourceCell;
    }

}
