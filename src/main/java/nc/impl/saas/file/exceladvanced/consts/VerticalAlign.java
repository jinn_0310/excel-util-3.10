package nc.impl.saas.file.exceladvanced.consts;

/**
 * 垂直位置
 *
 * @author xyyuanj
 */

public enum VerticalAlign {
    /**
     * 垂直位置
     */
    TOP((short) 0, "垂直居顶"),
    CENTER((short) 1, "垂直居中"),
    BOTTOM((short) 2, "垂直居底"),
    JUSTIFY((short) 3, "垂直对齐");

    private short idx;
    private String zhDesc;

    VerticalAlign(short idx, String zhDesc) {
        this.idx = idx;
        this.zhDesc = zhDesc;
    }

    public short getIdx() {
        return idx;
    }

    public String getZhDesc() {
        return zhDesc;
    }
}
