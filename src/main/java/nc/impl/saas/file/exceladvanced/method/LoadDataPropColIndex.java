package nc.impl.saas.file.exceladvanced.method;

import nc.impl.saas.file.exceladvanced.dto.SourceHeaderCell;

/**
 * @author xyyuanj
 */
public interface LoadDataPropColIndex {
    /**
     * 载入表头对应的列下标
     *
     * @param sourceHeaderCell 表头
     * @param colIndex         在Excel中的列下标
     */
    void load(SourceHeaderCell sourceHeaderCell, Integer colIndex);
}
