package nc.impl.saas.file.exceladvanced.tool;

import nc.impl.saas.file.exceladvanced.dto.SourceCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存
 */
public class CacheTool {
    private static final Unsafe unsafe = getUnsafe();
    private static final String LOCK_1 = "LOCK_1_" + System.currentTimeMillis();
    private static final String LOCK_2 = "LOCK_2_" + System.currentTimeMillis();
    private static ConcurrentHashMap<String, ConcurrentHashMap<String, CellStyle>> cellStyleCacheMap = new ConcurrentHashMap<>();

    public static CellStyle getCellStyle(Workbook workbook, SourceCell sourceCell) {
        try {
            String key1 = getAddresses(workbook);
            String key2 = sourceCell.getIdentify();
            ConcurrentHashMap<String, CellStyle> cacheMap = cellStyleCacheMap.get(key1);
            if (cacheMap == null) {
                synchronized (LOCK_1) {
                    cacheMap = cellStyleCacheMap.get(key1);
                    if (cacheMap == null) {
                        cellStyleCacheMap.put(key1, new ConcurrentHashMap<>());
                        cacheMap = cellStyleCacheMap.get(key1);
                    }
                    loadCellStyle(workbook, sourceCell, key2, cacheMap);
                }
            } else {
                loadCellStyle(workbook, sourceCell, key2, cacheMap);
            }

            return cacheMap.get(key2);
        } catch (Exception e) {
            return createCellStyle(workbook, sourceCell);
        }
    }
    
    public static void clear(Workbook workbook) {
        String key1 = getAddresses(workbook);
        cellStyleCacheMap.remove(key1);
    }

    public static <T> String getAddresses(T... objects) {
        long offset = unsafe.arrayBaseOffset(objects.getClass());
        long factor = 8;
        final long i1 = (unsafe.getInt(objects, offset) & 0xFFFFFFFFL) * factor;
        return "0x" + Long.toHexString(i1);
    }

    private static Unsafe getUnsafe() {
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            return (Unsafe) theUnsafe.get(null);
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    private static void loadCellStyle(Workbook workbook, SourceCell sourceCell, String key2, ConcurrentHashMap<String, CellStyle> cacheMap) {
        CellStyle cellStyle = cacheMap.get(key2);
        if (cellStyle == null) {
            synchronized (LOCK_2) {
                cellStyle = cacheMap.get(key2);
                if (cellStyle == null) {
                    cellStyle = createCellStyle(workbook, sourceCell);
                    cacheMap.put(key2, cellStyle);
                }
            }
        }
    }


    private static CellStyle createCellStyle(Workbook workbook, SourceCell sourceCell) {

        CellStyle style = workbook.createCellStyle();
        // 水平位置
        style.setAlignment(sourceCell.getAlign().getIdx());
        // 垂直位置
        style.setVerticalAlignment(sourceCell.getVerticalAlign().getIdx());
        // 缩进
        style.setIndention(sourceCell.getIndention());
        // 自定义格式
        if (sourceCell.getCustomDataFormat() != null && sourceCell.getCustomDataFormat().length() > 0) {
            style.setDataFormat(workbook.createDataFormat().getFormat(sourceCell.getCustomDataFormat()));
        }
        // 填充色
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(sourceCell.getFillForegroundColor().getIdx());
        // 边框-上
        style.setBorderTop(CellStyle.BORDER_THIN);
        // 边框-右
        style.setBorderRight(CellStyle.BORDER_THIN);
        // 边框-下
        style.setBorderBottom(CellStyle.BORDER_THIN);
        // 边框-左
        style.setBorderLeft(CellStyle.BORDER_THIN);
        // 换行
        style.setWrapText(sourceCell.getWrapText().isTrue());
        // 字体
        Font font = workbook.createFont();
        // 字体-名称
        font.setFontName(sourceCell.getFontName());
        // 字体大小
        font.setFontHeightInPoints(sourceCell.getFontSize());
        // 字体-加粗
        if (sourceCell.getBold().isTrue()) {
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        style.setFont(font);

        return style;
    }
}
