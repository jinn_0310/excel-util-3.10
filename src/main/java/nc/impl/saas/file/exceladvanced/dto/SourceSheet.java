package nc.impl.saas.file.exceladvanced.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 输入源：Sheet配置
 *
 * @author xyyuanj
 */
public class SourceSheet {

    private String sheetName;

    private List<SourceTable> sourceTableList;

    public SourceSheet() {
        this.sourceTableList = new ArrayList<>();
    }

    public SourceSheet(String sheetName) {
        this();
        this.sheetName = sheetName;
    }

    public SourceSheet(String sheetName, List<SourceTable> sourceTableList) {
        this();
        this.sheetName = sheetName;
        this.sourceTableList = sourceTableList;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public List<SourceTable> getSourceTableList() {
        return sourceTableList;
    }

    public void setSourceTableList(List<SourceTable> sourceTableList) {
        this.sourceTableList = sourceTableList;
    }

    public SourceTable createSourceTable() {
        this.sourceTableList.add(new SourceTable());
        return this.sourceTableList.get(this.sourceTableList.size() - 1);
    }
}
