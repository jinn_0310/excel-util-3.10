package nc.impl.saas.file.exceladvanced.consts;

/**
 * 逻辑是否
 *
 * @author xyyuanj
 */

public enum Logic {
    /**
     * 逻辑是否
     */
    YES(true, "是"),
    NO(false, "否");
    private boolean isTrue;
    private String zhDesc;

    Logic(boolean isTrue, String zhDesc) {
        this.isTrue = isTrue;
        this.zhDesc = zhDesc;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public String getZhDesc() {
        return zhDesc;
    }
}
