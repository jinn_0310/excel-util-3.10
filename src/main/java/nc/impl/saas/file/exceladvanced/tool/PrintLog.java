package nc.impl.saas.file.exceladvanced.tool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrintLog {
    public static void println(String fmt, Object... args) {
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
        String info = fmt;

        if (args != null && args.length > 0) {
            for (Object arg : args) {
                info = info.replace("{}", String.valueOf(arg));
            }
        }
        
        System.out.format("%s - %s\n", date, info);
    }

    public static void println(String info) {
        println(info, "");
    }
}
