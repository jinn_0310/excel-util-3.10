package nc.impl.saas.file.exceladvanced.dto;

import nc.impl.saas.file.exceladvanced.consts.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 输入源：Sheet中的表格
 *
 * @author xyyuanj
 */
public class SourceTable {
    /**
     * 前置保留空行数
     */
    private Integer prependEmptyRowNum;
    /**
     * 后置保留空行数
     */
    private Integer appendEmptyRowNum;
    /**
     * 表头
     */
    private List<SourceHeaderCell> sourceHeaderCellList;
    /**
     * 表体
     */
    private List<SourceRow> sourceBodyList;
    /**
     * 附加项
     */
    private List<List<SourceCell>> sourceAdditionalList;

    public SourceTable() {
        prependEmptyRowNum = 0;
        appendEmptyRowNum = 0;
        sourceHeaderCellList = new ArrayList<>();
        sourceBodyList = new ArrayList<>();
        sourceAdditionalList = new ArrayList<>();
    }

    public Integer getPrependEmptyRowNum() {
        return prependEmptyRowNum;
    }

    public void setPrependEmptyRowNum(Integer prependEmptyRowNum) {
        this.prependEmptyRowNum = prependEmptyRowNum;
    }

    public Integer getAppendEmptyRowNum() {
        return appendEmptyRowNum;
    }

    public void setAppendEmptyRowNum(Integer appendEmptyRowNum) {
        this.appendEmptyRowNum = appendEmptyRowNum;
    }

    public List<SourceHeaderCell> getSourceHeaderCellList() {
        return sourceHeaderCellList;
    }

    public void setSourceHeaderCellList(List<SourceHeaderCell> sourceHeaderCellList) {
        this.sourceHeaderCellList = sourceHeaderCellList;
    }

    public List<SourceRow> getSourceBodyList() {
        return sourceBodyList;
    }

    public void setSourceBodyList(List<SourceRow> sourceBodyList) {
        this.sourceBodyList = sourceBodyList;
    }

    public List<List<SourceCell>> getSourceAdditionalList() {
        return sourceAdditionalList;
    }

    public void setSourceAdditionalList(List<List<SourceCell>> sourceAdditionalList) {
        this.sourceAdditionalList = sourceAdditionalList;
    }

    public SourceTable addSourceHeaderCell(String dataProp, String value) {
        this.sourceHeaderCellList.add(new SourceHeaderCell(dataProp, value));
        return this;
    }

    public SourceTable addSourceHeaderCell(String value) {
        return addSourceHeaderCell(null, value);
    }

    public SourceHeaderCell createSourceHeaderCell(String value) {
        final SourceHeaderCell headerCell = new SourceHeaderCell(value);
        this.sourceHeaderCellList.add(headerCell);
        return headerCell;
    }

    public SourceHeaderCell createSourceHeaderCell(String value, Color fillForegroundColor) {
        final SourceHeaderCell headerCell = new SourceHeaderCell(value)
                .setFillForegroundColor(fillForegroundColor);
        this.sourceHeaderCellList.add(headerCell);
        return headerCell;
    }

    public SourceTable addSourceBodyRow(SourceRow sourceRow) {
        this.sourceBodyList.add(sourceRow);
        return this;
    }

    public SourceRow createSourceBodyRow() {
        SourceRow sourceRow = new SourceRow();
        this.sourceBodyList.add(sourceRow);
        return sourceRow;
    }

    public SourceTable addSourceAdditionalRow(SourceCell... cells) {
        List<SourceCell> row = new ArrayList<>();
        if (cells != null) {
            Collections.addAll(row, cells);
        }
        this.sourceAdditionalList.add(row);
        return this;
    }

}
