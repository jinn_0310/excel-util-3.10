package nc.impl.saas.file.exceladvanced.consts;

/**
 * 一些常量
 *
 * @author xyyuanj
 */
public class Const {
    public static final String SUFFIX_XLS = ".xls";
    public static final String SUFFIX_XLSX = ".xlsx";

    public static final short ROW_UNIT = 20;
    public static final short COL_UNIT = 256;
    public static final int MIN_COL_WIDTH = 10;
}
