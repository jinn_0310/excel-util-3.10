package nc.impl.saas.file.exceladvanced.dto;

import nc.impl.saas.file.exceladvanced.consts.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 输入源：头部：树结构
 *
 * @author xyyuanj
 */
public class SourceHeaderCell extends SourceCell {
    /**
     * 字段名称
     */
    private String dataProp;
    /**
     * 子级
     */
    private List<SourceHeaderCell> children;

    public SourceHeaderCell() {
        super();
        this.dataProp = null;
        this.children = new ArrayList<>();
        this.setFillForegroundColor(Color.GREY_25_PERCENT);
        this.setBold(Logic.YES);
    }

    public SourceHeaderCell(Object value) {
        this();
        this.setValue(value);
    }

    public SourceHeaderCell(String dataProp, Object value) {
        this();
        this.dataProp = dataProp;
        this.setValue(value);
    }

    public SourceHeaderCell(String dataProp, Object value, int width) {
        this();
        this.dataProp = dataProp;
        this.setValue(value);
        this.setWidth(width);
    }

    public String getDataProp() {
        return dataProp;
    }

    public void setDataProp(String dataProp) {
        this.dataProp = dataProp;
    }

    public List<SourceHeaderCell> getChildren() {
        return children;
    }

    public void setChildren(List<SourceHeaderCell> children) {
        this.children = children;
    }

    @Override
    public SourceHeaderCell setHeight(Integer height) {
        super.setHeight(height);
        return this;
    }

    @Override
    public SourceHeaderCell setValue(Object value) {
        super.setValue(value);
        return this;
    }

    @Override
    public SourceHeaderCell setWidth(Integer width) {
        super.setWidth(width);
        return this;
    }

    @Override
    public SourceHeaderCell setRowspan(Integer rowspan) {
        super.setRowspan(rowspan);
        return this;
    }

    @Override
    public SourceHeaderCell setColspan(Integer colspan) {
        super.setColspan(colspan);
        return this;
    }

    @Override
    public SourceHeaderCell setFillForegroundColor(Color fillForegroundColor) {
        super.setFillForegroundColor(fillForegroundColor);
        return this;
    }

    @Override
    public SourceHeaderCell setWrapText(Logic wrapText) {
        super.setWrapText(wrapText);
        return this;
    }

    @Override
    public SourceHeaderCell setFontName(String fontName) {
        super.setFontName(fontName);
        return this;
    }

    @Override
    public SourceHeaderCell setFontSize(Short fontSize) {
        super.setFontSize(fontSize);
        return this;
    }

    @Override
    public SourceHeaderCell setBold(Logic bold) {
        super.setBold(bold);
        return this;
    }

    @Override
    public SourceHeaderCell setCustomDataFormat(String customDataFormat) {
        super.setCustomDataFormat(customDataFormat);
        return this;
    }

    @Override
    public SourceHeaderCell setIndention(Short indention) {
        super.setIndention(indention);
        return this;
    }

    @Override
    public SourceHeaderCell setDataType(DataType dataType) {
        super.setDataType(dataType);
        return this;
    }

    @Override
    public SourceHeaderCell setVerticalAlign(VerticalAlign verticalAlign) {
        super.setVerticalAlign(verticalAlign);
        return this;
    }

    @Override
    public SourceHeaderCell setAlign(Align align) {
        super.setAlign(align);
        return this;
    }

    @Override
    public SourceHeaderCell setMergeKey(String mergeKey) {
        super.setMergeKey(mergeKey);
        return this;
    }

    @Override
    public SourceHeaderCell setSplitFreeze(Logic splitFreeze) {
        super.setSplitFreeze(splitFreeze);
        return this;
    }

    /**
     * 当前单元格添加子级
     *
     * @param children 子级
     * @return 返回当前单元格
     */
    public SourceHeaderCell addChildren(SourceHeaderCell... children) {
        if (children != null) {
            Collections.addAll(this.children, children);
        }
        return this;
    }

    /**
     * 当前单元格添加子级
     *
     * @return 返回当前单元格
     */
    public SourceHeaderCell addChildren(String dataProp, String value) {
        final SourceHeaderCell childCell = new SourceHeaderCell(dataProp, value);
        this.children.add(childCell);
        return this;
    }

    /**
     * 当前单元格添加子级
     *
     * @return 子级单元格
     */
    public SourceHeaderCell createChildren(String value) {
        final SourceHeaderCell childCell = new SourceHeaderCell(value);
        this.children.add(childCell);
        return childCell;
    }

    /**
     * 当前单元格添加子级
     *
     * @return 子级单元格
     */
    public SourceHeaderCell createChildren(String dataProp, String value) {
        final SourceHeaderCell childCell = new SourceHeaderCell(dataProp, value);
        this.children.add(childCell);
        return childCell;
    }
}
