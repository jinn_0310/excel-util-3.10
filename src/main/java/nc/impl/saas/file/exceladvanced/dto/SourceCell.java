package nc.impl.saas.file.exceladvanced.dto;

import nc.impl.saas.file.exceladvanced.consts.*;

import static nc.impl.saas.file.exceladvanced.consts.Const.MIN_COL_WIDTH;

/**
 * 输入源：单元格配置
 *
 * @author xyyuanj
 */
public class SourceCell {
    /**
     * 单元格值（必要）
     */
    private Object value;
    /**
     * 数据类型（非必要）
     */
    private DataType dataType;
    /**
     * 宽度（非必要）
     */
    private Integer width;
    /**
     * 高度（非必要）
     */
    private Integer height;
    /**
     * 是否以此单元格设置冻结单元格（非必要）
     */
    private Logic splitFreeze;
    /**
     * 单元格填充色（非必要）
     */
    private Color fillForegroundColor;
    /**
     * 单元格垂直位置（非必要）
     */
    private VerticalAlign verticalAlign;
    /**
     * 单元格水平位置（非必要）
     */
    private Align align;
    /**
     * 单元格是否支持换行(非必要）
     */
    private Logic wrapText;
    /**
     * 字体名称（非必要）
     */
    private String fontName;
    /**
     * 字体大小（非必要）
     */
    private Short fontSize;
    /**
     * 是否加粗
     */
    private Logic bold;
    /**
     * 自定义格式（非必要）
     */
    private String customDataFormat;
    /**
     * 对齐缩进量（非必要）
     */
    private Short indention;
    /**
     * 合并行数（非必要）
     */
    private Integer rowspan;
    /**
     * 合并列数（非必要）
     */
    private Integer colspan;
    /**
     * TODO: 尚未实现
     * 合并标志：相邻且相同时进行合并；与 colspan和rowspan 功能相同
     */
    private String mergeKey;

    public SourceCell() {
        this.value = "";
        this.dataType = DataType.CELL_TYPE_STRING;
        this.width = MIN_COL_WIDTH;
        // -1 表示默认不设置
        this.height = -1;
        this.splitFreeze = Logic.NO;
        this.fillForegroundColor = Color.WHITE;
        this.verticalAlign = VerticalAlign.CENTER;
        this.align = Align.CENTER;
        this.wrapText = Logic.NO;
        this.fontName = "宋体";
        this.fontSize = 11;
        this.bold = Logic.NO;
        this.indention = 0;
        this.rowspan = 1;
        this.colspan = 1;
        this.mergeKey = "";
    }

    public SourceCell(Object value) {
        this();
        this.value = value;
    }

    public SourceCell(Object value, DataType dataType) {
        this();
        this.value = value;
        this.dataType = dataType;
    }

    public SourceCell(SourceCell copy) {
        this.value = copy.value;
        this.dataType = copy.dataType;
        this.width = copy.width;
        this.height = copy.height;
        this.rowspan = copy.rowspan;
        this.colspan = copy.colspan;
        this.mergeKey = copy.mergeKey;
        this.splitFreeze = copy.splitFreeze;
        this.fillForegroundColor = copy.fillForegroundColor;
        this.verticalAlign = copy.verticalAlign;
        this.align = copy.align;
        this.wrapText = copy.wrapText;
        this.fontName = copy.fontName;
        this.fontSize = copy.fontSize;
        this.bold = copy.bold;
        this.customDataFormat = copy.customDataFormat;
        this.indention = copy.indention;
    }

    public Integer getHeight() {
        return height;
    }

    public SourceCell setHeight(Integer height) {
        this.height = height;
        return this;
    }

    public Object getValue() {
        return value == null ? "" : value;
    }

    public SourceCell setValue(Object value) {
        this.value = value;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public SourceCell setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public Integer getRowspan() {
        return rowspan;
    }

    public SourceCell setRowspan(Integer rowspan) {
        this.rowspan = rowspan;
        return this;
    }

    public Integer getColspan() {
        return colspan;
    }

    public SourceCell setColspan(Integer colspan) {
        this.colspan = colspan;
        return this;
    }

    public Color getFillForegroundColor() {
        return fillForegroundColor;
    }

    public SourceCell setFillForegroundColor(Color fillForegroundColor) {
        this.fillForegroundColor = fillForegroundColor;
        return this;
    }

    public Logic getWrapText() {
        return wrapText;
    }

    public SourceCell setWrapText(Logic wrapText) {
        this.wrapText = wrapText;
        return this;
    }

    public String getFontName() {
        return fontName;
    }

    public SourceCell setFontName(String fontName) {
        this.fontName = fontName;
        return this;
    }

    public Short getFontSize() {
        return fontSize;
    }

    public SourceCell setFontSize(Short fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public Logic getBold() {
        return bold;
    }

    public SourceCell setBold(Logic bold) {
        this.bold = bold;
        return this;
    }

    public String getCustomDataFormat() {
        return customDataFormat;
    }

    public SourceCell setCustomDataFormat(String customDataFormat) {
        this.customDataFormat = customDataFormat;
        return this;
    }

    public Short getIndention() {
        return indention;
    }

    public SourceCell setIndention(Short indention) {
        this.indention = indention;
        return this;
    }

    public DataType getDataType() {
        return dataType;
    }

    public SourceCell setDataType(DataType dataType) {
        this.dataType = dataType;
        return this;
    }

    public VerticalAlign getVerticalAlign() {
        return verticalAlign;
    }

    public SourceCell setVerticalAlign(VerticalAlign verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    public Align getAlign() {
        return align;
    }

    public SourceCell setAlign(Align align) {
        this.align = align;
        return this;
    }

    public String getMergeKey() {
        return mergeKey;
    }

    public SourceCell setMergeKey(String mergeKey) {
        this.mergeKey = mergeKey;
        return this;
    }

    public Logic getSplitFreeze() {
        return splitFreeze;
    }

    public SourceCell setSplitFreeze(Logic splitFreeze) {
        this.splitFreeze = splitFreeze;
        return this;
    }
    
    public String getIdentify() {
        return "SourceCell{" +
                "dataType=" + dataType +
                ", width=" + width +
                ", height=" + height +
                ", splitFreeze=" + splitFreeze +
                ", fillForegroundColor=" + fillForegroundColor +
                ", verticalAlign=" + verticalAlign +
                ", align=" + align +
                ", wrapText=" + wrapText +
                ", fontName='" + fontName + '\'' +
                ", fontSize=" + fontSize +
                ", bold=" + bold +
                ", customDataFormat='" + customDataFormat + '\'' +
                ", indention=" + indention +
                ", rowspan=" + rowspan +
                ", colspan=" + colspan +
                ", mergeKey='" + mergeKey + '\'' +
                '}';
    }

    @Override
    public String toString() {
        return "SourceCell{" +
                "value=" + value +
                ", dataType=" + dataType +
                ", width=" + width +
                ", height=" + height +
                ", splitFreeze=" + splitFreeze +
                ", fillForegroundColor=" + fillForegroundColor +
                ", verticalAlign=" + verticalAlign +
                ", align=" + align +
                ", wrapText=" + wrapText +
                ", fontName='" + fontName + '\'' +
                ", fontSize=" + fontSize +
                ", bold=" + bold +
                ", customDataFormat='" + customDataFormat + '\'' +
                ", indention=" + indention +
                ", rowspan=" + rowspan +
                ", colspan=" + colspan +
                ", mergeKey='" + mergeKey + '\'' +
                '}';
    }
}
