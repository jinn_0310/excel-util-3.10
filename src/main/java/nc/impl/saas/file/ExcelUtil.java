package nc.impl.saas.file;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @version 2.0 支持复杂单元格导出，支持各种自定义功能，详细见<code>README.md</code>
 */
public class ExcelUtil {
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_NUMERIC = 0;
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_STRING = 1;
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_FORMULA = 2;
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_BLANK = 3;
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_BOOLEAN = 4;
    @SuppressWarnings("unused")
    public static final int CELL_TYPE_ERROR = 5;

    // 以此单元格为设置"冻结窗格"
    public static final String ENABLE_SPLIT_FREEZE = "Y";

    /**
     * 最大列宽
     */
    private static final int MAX_COLUMN_WIDTH = 216 * 40;

    public static String read(File file) {
        return read(file, StandardCharsets.UTF_8.name());
    }
    public static String read(File file, final String charset) {
        try {
            if (isFile(file.getAbsolutePath())) {
                FileInputStream fis = new FileInputStream(file);
                return read(fis, charset);
            }
            return null;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public static boolean isFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            return file.isFile();
        }
        return Boolean.FALSE;
    }
    public static String read(InputStream inputStream, final String charset) {
        String result = null;
        if (null != inputStream) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                byte[] bytes = new byte[1024];
                int length;
                while ((length = inputStream.read(bytes)) != -1) {
                    bos.write(bytes, 0, length);
                }
                result = bos.toString(charset);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close(bos);
            }
        }
        return result;
    }

    /**
     * 如果不存在则创建文件
     *
     * @param filePath 文件绝对路径
     * @return 创建的文件
     */
    public static File createFileIfNotExists(String filePath) {
        File file = new File(filePath);
        try {
            if (!file.exists()) {
                File parentFile = file.getParentFile();
                if (parentFile.mkdirs() || parentFile.exists()) {
                    if (file.createNewFile()) {
                        return file;
                    } else {
                        file = null;
                    }
                } else {
                    file = null;
                }
            }
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 写入字符串
     *
     * @param content  内容
     * @param charset  编码
     * @param filePath 保存文件的绝对路径
     * @return 成功返回 File, 否则 null
     */
    public static File write(String filePath, final String charset, String content, boolean append) {
        File file = createFileIfNotExists(filePath);
        FileOutputStream fos = null;
        try {
            if (file != null) {
                fos = new FileOutputStream(file, append);
                fos.write(content.getBytes(charset));
                fos.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
            file = null;
        } finally {
            close(fos);
        }

        return file;
    }

    public static void close(InputStream... inputStreams) {
        if (inputStreams != null) {
            for (InputStream stream : inputStreams) {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void close(OutputStream... outputStreams) {
        if (outputStreams != null) {
            for (OutputStream stream : outputStreams) {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void buildExcelFromSourceData(List<SourceData> sourceDataList, String targetPath) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(targetPath));
        buildExcelFromSourceData(sourceDataList, fileOutputStream);
    }

    public static void buildExcelFromSourceData(List<SourceData> sourceDataList, OutputStream outputStream) throws IOException {
        HSSFWorkbook wb = new HSSFWorkbook();

        // 把内置的灰色（25%）替换成自定义的颜色（烟灰色）
        HSSFPalette customPalette = wb.getCustomPalette();
        customPalette.setColorAtIndex(IndexedColors.GREY_25_PERCENT.getIndex(), (byte) 245, (byte) 245, (byte) 245);

        for (SourceData sourceData : sourceDataList) {
            buildExcelSheet(sourceData, wb, sourceData.getSheetName());
        }

        wb.write(outputStream);
    }

    /**
     * 创建工作表，并写入数据
     *
     * @param sourceData 源数据
     * @param wb         工作薄
     * @param sheetName  工作表
     */
    private static void buildExcelSheet(SourceData sourceData, HSSFWorkbook wb, String sheetName) {
        HSSFSheet sheet = wb.createSheet(sheetName);
        sheet.autoSizeColumn(1, true);

        /*
         * 1. 格式化Excel头部数据（合并行，合并列）
         */
        List<RowData> headerRowDataList = transToHeaderRowDataList(sourceData.getHeaderList());

//        /*
//         * 2. 设置每列数据的字段取值(已经排序)
//         */
//        List<String> dataPropList = new ArrayList<>();
//        // 值的类型
//        Map<String, Integer> dataTypeMap = new HashMap<>();
//        recursionColDataProp(dataPropList, dataTypeMap, sourceData.getHeaderList());

        int curRowIndex = 0;
        /*
         * 3. 写入标题
         */
        curRowIndex = writeHeaderListToSheet(wb, sheet, curRowIndex, headerRowDataList);
        /*
         * 设置冻结
         * 第一个参数表示要冻结的列数，从0开始，这里的0表示不设置冻结。
         * 第二个参数表示要冻结的行数，从0开始，这里的0表示不设置冻结。
         * 第三个参数表示在右侧窗格中可以看见的冻结列数，从1开始。
         * 第四个参数表示在底部窗格中可以看见的冻结行数，从1开始。
         */
        sheet.createFreezePane(0, headerRowDataList.size(), 0, headerRowDataList.size());

        /*
         * 4. 写入数据
         */
        // 4.1 将body数据转换成 RowData，写入Sheet
        List<RowData> dataList = transToRowDataList(sourceData.getDataObjList());
        curRowIndex = writeDataListToSheet(wb, sheet, curRowIndex, dataList);

        // 4.2 写入末尾数据
        List<RowData> footList = transToRowDataList(sourceData.getFootObjList());
        curRowIndex = writeFootListToSheet(wb, sheet, curRowIndex, footList);

    }

    /**
     * 按列顺序获取表头中对应的数据字段名成
     *
     * @param dataPropList 数据字段名称列表
     * @param dataTypeMap  数据字段类型映射
     * @param headerList   表头源数据
     */
    private static void recursionColDataProp(List<String> dataPropList, Map<String, Integer> dataTypeMap, List<Header> headerList) {
        for (Header header : headerList) {
            if (null == header.getChildren() || header.getChildren().size() == 0) {
                dataPropList.add(header.getDataProp());
                if (null == header.getDataType()) {
                    dataTypeMap.put(header.getDataProp(), Cell.CELL_TYPE_STRING);
                } else {
                    dataTypeMap.put(header.getDataProp(), header.getDataType());
                }
            } else {
                recursionColDataProp(dataPropList, dataTypeMap, header.getChildren());
            }
        }
    }

    /**
     * 转换表头源数据为可写入Excel的行数据
     *
     * @param headerList 表头源数据
     * @return 可写入Excel的行数据
     */
    private static List<RowData> transToHeaderRowDataList(List<Header> headerList) {
        /*
         * 1. 格式化列合并
         */
        List<CellData> cellDataList = new ArrayList<>();
        recursionHeader(cellDataList, headerList, 0, 0);

        /*
         * 2. 格式化行合并
         */
        // 2.1 按行分组
        Map<Integer, List<CellData>> rowDataMap = new TreeMap<>();
        for (CellData cellData : cellDataList) {
            int row = cellData.getStartRow();
            if (rowDataMap.get(row) == null) {
                rowDataMap.put(row, new ArrayList<CellData>());
            }
            rowDataMap.get(row).add(cellData);
        }
        // 2.2 统计每行缺少的列标
        // 获取最大列标
        Integer maxColIndex = getMaxColIndex(cellDataList);
        Map<Integer, List<String>> rowLackColMap = new HashMap<>(); // 统计每行缺少的列标
        for (Map.Entry<Integer, List<CellData>> entry : rowDataMap.entrySet()) {
            int row = entry.getKey();
            List<CellData> rowCellDataList = entry.getValue();
            List<String> colIndexList = getColIndexList(maxColIndex);
            for (CellData cellData : rowCellDataList) {
                for (int i = cellData.getStartCol(); i <= cellData.getEndCol(); i++) {
                    colIndexList.remove(String.valueOf(i));
                }
            }
            if (rowCellDataList.size() > 0) {
                rowLackColMap.put(row, colIndexList);
            }
        }
        // 2.3 格式化行
        for (Map.Entry<Integer, List<String>> entry : rowLackColMap.entrySet()) {
            int row = entry.getKey();
            List<String> lackColList = entry.getValue();
            for (String colStr : lackColList) {
                int col = Integer.parseInt(colStr);

                for (Map.Entry<Integer, List<CellData>> rowEntry : rowDataMap.entrySet()) {
                    List<CellData> fillCellDataList = rowEntry.getValue();
                    if (null != fillCellDataList && fillCellDataList.size() > 0) {
                        for (CellData cellData : fillCellDataList) {
                            if (cellData.getStartCol() >= col && cellData.getEndCol() <= col) {
                                cellData.setEndRow(row);
                            }
                        }
                    }
                }
            }
        }

        List<RowData> rowDataList = new ArrayList<>();
        for (Map.Entry<Integer, List<CellData>> entry : rowDataMap.entrySet()) {
            RowData rowData = new RowData();
            rowData.setRowIndex(entry.getKey());
            rowData.setCellDataList(entry.getValue());
            rowDataList.add(rowData);
        }
        return rowDataList;
    }

    /**
     * 获取最大的列下标
     *
     * @param cellDataList 单元格数据集合
     * @return 列下标
     */
    private static Integer getMaxColIndex(List<CellData> cellDataList) {
        int maxColIndex = 0;
        for (CellData cellData : cellDataList) {
            if (maxColIndex < cellData.getEndCol()) {
                maxColIndex = cellData.getEndCol();
            }
        }
        return maxColIndex;
    }

    private static List<String> getColIndexList(Integer endCol) {
        List<String> list = new ArrayList<>();
        for (Integer i = 0; i <= endCol; i++) {
            list.add(String.valueOf(i));
        }
        return list;
    }

    /**
     * 递归格式化表头数据，解析列合并
     *
     * @param cellDataList 单元格数据（输出数据）
     * @param headerList   表头数据（源数据）
     * @param startCol     表头开始列
     * @param startRow     开始行
     * @return 列下标
     */
    private static Integer recursionHeader(List<CellData> cellDataList, List<Header> headerList, int startCol, int startRow) {
        if (headerList != null && headerList.size() > 0) {
            for (Header header : headerList) {
                CellData cellData = new CellData();
                cellData.setStartRow(startRow);
                cellData.setEndRow(startRow); // 初始化行号
                cellData.setStartCol(startCol);
                cellData.setValue(header.getTitle());
                cellData.setDataProp(header.getDataProp());
                cellData.setVerticalAlign(header.getVerticalAlign());
                cellData.setAlign(header.getAlign());
                cellData.setFontName(header.getFontName());
                cellData.setFontSize(header.getFontSize());
                cellData.setWrapText(header.getWrapText());
                cellData.setFillForegroundColor(header.getFillForegroundColor());
                if (header.getChildren() != null && header.getChildren().size() > 0) {
                    startCol = recursionHeader(cellDataList, header.getChildren(), startCol, startRow + 1);
                    cellData.setEndCol(--startCol);
                } else {
                    cellData.setEndCol(startCol);
                }
                startCol++;
                cellDataList.add(cellData);
            }
        }
        return startCol;
    }

    /**
     * 写入尾部数据
     */
    private static int writeFootListToSheet(HSSFWorkbook wb, HSSFSheet sheet, int curRowIndex, List<RowData> dataList) {
        StyleParam styleParam = new StyleParam();
        styleParam.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        HSSFCellStyle footStyle = createCellStyle(wb, styleParam);
        return writeRowListToSheet(wb, sheet, footStyle, curRowIndex, dataList);
    }

    /**
     * 写入行数据（非表头）
     *
     * @param wb               工作薄
     * @param sheet            工作表
     * @param defaultCellStyle 默认单元格样式
     * @param curRowIndex      开始写入行下标
     * @param dataList         行数据
     * @return 行下标
     */
    private static int writeRowListToSheet(HSSFWorkbook wb, HSSFSheet sheet, HSSFCellStyle defaultCellStyle, int curRowIndex, List<RowData> dataList) {
        if (dataList.size() > 0) {
            boolean alreadyFreeze = false;
            curRowIndex++;
            // 4.1 写入body数据
            for (RowData rowData : dataList) {
                HSSFRow row = sheet.getRow(curRowIndex + rowData.getRowIndex());
                if (null == row) {
                    row = sheet.createRow(curRowIndex + rowData.getRowIndex());
                }
                for (CellData cellData : rowData.getCellDataList()) {
                    // 写入每个数据单元格
                    for (Integer rowIndex = cellData.getStartRow(); rowIndex <= cellData.getEndRow(); rowIndex++) {
                        HSSFRow tmpRow = sheet.getRow(curRowIndex + rowData.getRowIndex());
                        if (null == tmpRow) {
                            tmpRow = sheet.createRow(curRowIndex + rowData.getRowIndex());
                        }
                        for (int colIndex = cellData.getStartCol(); colIndex <= cellData.getEndCol(); colIndex++) {
                            HSSFCell cell = tmpRow.getCell(colIndex);
                            if (null == cell) {
                                Object val = cellData.getValue();
                                cell = row.createCell(colIndex);
                                if (null == cellData.getDataType()) {
                                    if (isNotEmptyString(val)) {
                                        if (val instanceof Integer) {
                                            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                                            cell.setCellValue(Integer.parseInt(stringValueOf(val)));
                                        } else if (val instanceof Double) {
                                            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                                            cell.setCellValue(Double.parseDouble(stringValueOf(val)));
                                        } else {
                                            cell.setCellType(Cell.CELL_TYPE_STRING);
                                            cell.setCellValue(stringValueOf(val));
                                        }
                                    } else {
                                        cell.setCellValue("");
                                    }
                                } else {
                                    cell.setCellType(cellData.getDataType());
                                    if (Cell.CELL_TYPE_NUMERIC == cellData.getDataType()) {
                                        if (isNotEmptyString(val)) {
                                            try {
                                                cell.setCellValue(Double.parseDouble(stringValueOf(val)));
                                            } catch (NumberFormatException e) {
                                                cell.setCellValue(stringValueOf(val));
                                            }
                                        } else {
                                            cell.setCellValue("");
                                        }
                                    } else {
                                        if (isNotEmptyString(val)) {
                                            cell.setCellValue(stringValueOf(val));
                                        } else {
                                            cell.setCellValue("");
                                        }
                                    }
                                }
                                if (cellData.hasCustomCellStyle()) {
                                    StyleParam styleParam = new StyleParam(cellData);
                                    HSSFCellStyle style = createCellStyle(wb, styleParam);
                                    cell.setCellStyle(style);
                                } else {
                                    cell.setCellStyle(defaultCellStyle);
                                }

                                setColumnWidth(sheet, colIndex, val);
                            }
                        }
                    }
                    if (cellData.isNeedMerge()) {
                        // 添加合并样式
                        CellRangeAddress cra = new CellRangeAddress(cellData.getStartRow() + curRowIndex, cellData.getEndRow() + curRowIndex, cellData.getStartCol(), cellData.getEndCol());
                        sheet.addMergedRegion(cra);
                    }
                    if (!alreadyFreeze && cellData.isSplitFreeze()) {
                        alreadyFreeze = true;
                        // 设置单元格，冻结窗格
                        int colSplit = cellData.getStartCol();
                        int rowSplit = curRowIndex + cellData.getStartRow();
                        int leftMostColumn = cellData.getStartCol();
                        int topRow = curRowIndex + cellData.getStartRow();
                        sheet.createFreezePane(colSplit, rowSplit, leftMostColumn, topRow);
                    }
                }
            }
            curRowIndex += dataList.get(dataList.size() - 1).getRowIndex();
        }
        return curRowIndex;
    }

    /**
     * 创建单元格样式
     *
     * @param wb         工作簿
     * @param styleParam {@link StyleParam} 为null时，一律使用默认值，默认无填充色
     * @return HSSFCellStyle
     */
    private static HSSFCellStyle createCellStyle(HSSFWorkbook wb, StyleParam styleParam) {
        if (null == styleParam) {
            styleParam = new StyleParam();
        }
        // 样式 - 数据单元格
        HSSFCellStyle dataStyle = wb.createCellStyle();
        dataStyle.setAlignment(styleParam.getAlign()); // 创建一个居中格式 
        dataStyle.setVerticalAlignment(styleParam.getVerticalAlign());
        if (null != styleParam.getFillForegroundColor()) {
            dataStyle.setFillForegroundColor(styleParam.getFillForegroundColor());
            dataStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        } else {
            //dataStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
            dataStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        }
        dataStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框 
        dataStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN); //左边框 
        dataStyle.setBorderTop(HSSFCellStyle.BORDER_THIN); //上边框 
        dataStyle.setBorderRight(HSSFCellStyle.BORDER_THIN); //右边框 
        dataStyle.setWrapText(styleParam.getWrapText());
        HSSFFont dataFont = wb.createFont();
        dataFont.setFontName(styleParam.getFontName());
        if (styleParam.getBold()) {
            dataFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        }
//        dataFont.setFontHeightInPoints(styleParam.getFontSize()); //设置字体大小 
        dataStyle.setFont(dataFont);
        return dataStyle;
    }

    /**
     * 写入表头
     *
     * @param wb                工作簿
     * @param sheet             工作表
     * @param curRowIndex       开始写入行下标
     * @param headerRowDataList 表头数据
     * @return 行下标
     */
    private static int writeHeaderListToSheet(HSSFWorkbook wb, HSSFSheet sheet, int curRowIndex, List<RowData> headerRowDataList) {
        StyleParam styleParam = new StyleParam();
        styleParam.setBold(true);
        styleParam.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        HSSFCellStyle defaultHeaderStyle = createCellStyle(wb, styleParam);

        for (RowData rowData : headerRowDataList) {
            curRowIndex = rowData.getRowIndex();
            for (CellData cellData : rowData.getCellDataList()) {
                for (int colIndex = cellData.getStartCol(); colIndex <= cellData.getEndCol(); colIndex++) {
                    for (Integer tmpRowIndex = cellData.getStartRow(); tmpRowIndex <= cellData.getEndRow(); tmpRowIndex++) {
                        HSSFRow tmpRow = sheet.getRow(tmpRowIndex);
                        if (null == tmpRow) {
                            tmpRow = sheet.createRow(tmpRowIndex);
                            tmpRow.setHeight((short) (19*20));
                        }
                        HSSFCell cell = tmpRow.createCell(colIndex);
                        String headerVal = stringValueOf(cellData.getValue());
                        cell.setCellValue(headerVal);
                        if (cellData.hasCustomCellStyle()) {
                            StyleParam styleParam1 = new StyleParam(cellData);
                            styleParam1.setBold(true);
                            HSSFCellStyle headerStyle = createCellStyle(wb, styleParam1);
                            cell.setCellStyle(headerStyle);
                        } else {
                            cell.setCellStyle(defaultHeaderStyle);
                        }
                        // formatColumnWidth(sheet, colIndex, headerVal);
                    }

                }
                if (cellData.isNeedMerge()) {
                    // 添加合并样式
                    CellRangeAddress cra = new CellRangeAddress(cellData.getStartRow(), cellData.getEndRow(), cellData.getStartCol(), cellData.getEndCol());
                    sheet.addMergedRegion(cra);
                }
            }
        }
        return curRowIndex;
    }

    /**
     * 写入数据体
     *
     * @param wb          HSSFWorkbook
     * @param sheet       HSSFSheet
     * @param curRowIndex 当前行索引
     * @param dataList    写入数据
     * @return 当前sheet行索引
     */
    private static int writeDataListToSheet(HSSFWorkbook wb, HSSFSheet sheet, int curRowIndex, List<RowData> dataList) {
//        HSSFCellStyle dataStyle = createCellStyle(wb, IndexedColors.AUTOMATIC.getIndex());

        StyleParam styleParam = new StyleParam();
        styleParam.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        HSSFCellStyle dataStyle = createCellStyle(wb, styleParam);
        return writeRowListToSheet(wb, sheet, dataStyle, curRowIndex, dataList);
    }

    /**
     * 转换源数据得到可写入Excel的行数据（非表头）
     */
    private static List<RowData> transToRowDataList(List<List<Data>> rowList) {
        List<RowData> rowDataList = new ArrayList<>();
        if (null == rowList || rowList.size() == 0) {
            return rowDataList;
        }

        // 格式化行数据，合并列、行
        for (int rowIndex = 0; rowIndex < rowList.size(); rowIndex++) {
            RowData rowData = new RowData();
            rowDataList.add(rowData);
            rowData.setRowIndex(rowIndex);
            rowData.setCellDataList(new ArrayList<CellData>());

            List<CellData> cellDataList = rowData.getCellDataList();

            List<Data> cellList = rowList.get(rowIndex);
            for (int colIndex = 0; colIndex < cellList.size(); colIndex++) {
                Data cell = cellList.get(colIndex);
                String key = cell.getKey();
                CellData cellData = new CellData();
                cellDataList.add(cellData);
                cellData.setSplitFreeze(ENABLE_SPLIT_FREEZE.equalsIgnoreCase(cell.getSplitFreeze()));
                cellData.setVerticalAlign(cell.getVerticalAlign());
                cellData.setAlign(cell.getAlign());
                cellData.setFontName(cell.getFontName());
                cellData.setFontSize(cell.getFontSize());
                cellData.setWrapText(cell.getWrapText());
                cellData.setFillForegroundColor(cell.getFillForegroundColor());
                cellData.setDataType(cell.getDataType());
                cellData.setValue(cell.getValue());
                cellData.setDataProp(cell.getDataProp());
                if (isEmptyString(key)) {
                    // 不合并
                    cellData.setStartRow(rowIndex);
                    cellData.setEndRow(rowIndex);
                    cellData.setStartCol(colIndex);
                    cellData.setEndCol(colIndex);
                } else {
                    // 合并列，合并后只需要添加一次
                    // 检查前后列是否有相同的key，设置开始/结束列标
                    // start - 合并列
                    Integer startCol = null;
                    Integer endCol = null;
                    for (int i = 0; i < cellList.size(); i++) {
                        String tmpKey = cellList.get(i).getKey();
                        if (i < colIndex) {
                            if (isEmptyString(tmpKey)) {
                                // 前面的列key为空则跳过
                                continue;
                            }
                            if (key.equals(tmpKey)) {
                                startCol = i;
                            }
                        } else if (i > colIndex) {
                            if (isEmptyString(tmpKey)) {
                                // 后面的列key为空，则不再检查
                                break;
                            }
                            if (key.equals(tmpKey)) {
                                endCol = i;
                            }
                        } else {
                            if (null == startCol) startCol = i;
                            if (null == endCol) endCol = i;
                        }
                    }
                    cellData.setStartCol(startCol);
                    cellData.setEndCol(endCol);
                    // end - 合并列

                    // start - 合并行
                    Integer startRow = null;
                    Integer endRow = null;
                    for (int i = 0; i < rowList.size(); i++) {
                        String tmpKey = rowList.get(i).get(colIndex).getKey();
                        if (i < rowIndex) {
                            if (isEmptyString(tmpKey)) {
                                continue;
                            }
                            if (key.equals(tmpKey)) {
                                startRow = i;
                            }
                        } else if (i > rowIndex) {
                            if (isEmptyString(tmpKey)) {
                                // 后面的行key为空，则不再检查
                                break;
                            }
                            if (key.equals(tmpKey)) {
                                endRow = i;
                            }
                        } else {
                            if (null == startRow) startRow = i;
                            if (null == endRow) endRow = i;
                        }
                    }
                    cellData.setStartRow(startRow);
                    cellData.setEndRow(endRow);
                    // end - 合并行

                    if (null != endCol) {
                        colIndex = endCol;
                    }
                }
            }
        }

        return rowDataList;
    }

    /**
     * 设置列宽度
     *
     * @param sheet     工作表
     * @param colIndex  单元格的列下标
     * @param cellValue 单元格的值
     */
    private static void setColumnWidth(HSSFSheet sheet, int colIndex, Object cellValue) {
        String valueStr = stringValueOf(cellValue);
        if (isEmptyString(valueStr)) {
            return;
        }
        int columnWidth = sheet.getColumnWidth(colIndex);
        int curCellColWidth = Double.valueOf(216 * valueStr.getBytes().length * 1.0).intValue();
        // System.out.printf("%s | 宽度[%s] = bytes[%s] = %s\n", sheet.getSheetName(), curCellColWidth, valueStr.getBytes().length, valueStr);
        if (MAX_COLUMN_WIDTH >= curCellColWidth) {
            if (curCellColWidth > columnWidth) {
                sheet.setColumnWidth(colIndex, curCellColWidth);
            }
        } else {
            sheet.setColumnWidth(colIndex, MAX_COLUMN_WIDTH);
        }
    }

    private static boolean isEmptyString(Object val) {
        return null == val || String.valueOf(val).length() == 0;
    }

    private static boolean isNotEmptyString(Object val) {
        return !isEmptyString(val);
    }

    private static String stringValueOf(Object val) {
        if (null == val) {
            return "";
        }
        return String.valueOf(val);
    }

    /**
     * 输入源 - 表格头部
     */
    public static class Header {
        /**
         * 表头单元格的值（必要）
         */
        private String title;
        /**
         * 唯一标识（非必要）
         */
        private String titleKey;
        /**
         * 数据的字段名（必要）
         */
        private String dataProp;
        /**
         * 单元格类型（必要）
         * 见 <code>ExcelUtil.CELL_TYPE_*</code>
         */
        private Integer dataType;
        /**
         * 单元格填充色（非必要）
         */
        private Short fillForegroundColor;
        /**
         * 单元格垂直位置（非必要）
         * 见：HSSFCellStyle.VERTICAL_*
         */
        private Short verticalAlign;
        /**
         * 单元格水平位置（非必要）
         * 见：HSSFCellStyle.ALIGN_*
         */
        private Short align;
        /**
         * 单元格是否支持换行(非必要）
         */
        private Boolean wrapText;
        /**
         * 字体名称（非必要）
         */
        private String fontName;
        /**
         * 字体大小（非必要）
         */
        private Short fontSize;
        /**
         * 子级标题
         */
        private List<Header> children;

        public Header() {
        }

        public Header(String title, String titleKey, String dataProp) {
            this.title = title;
            this.titleKey = titleKey;
            this.dataProp = dataProp;
        }

        public Header(String title, String titleKey, List<Header> children) {
            this.title = title;
            this.titleKey = titleKey;
            this.children = children;
        }

        public Header(String title, String titleKey, String dataProp, List<Header> children) {
            this.title = title;
            this.titleKey = titleKey;
            this.dataProp = dataProp;
            this.children = children;
        }

        public Boolean getWrapText() {
            return wrapText;
        }

        public void setWrapText(Boolean wrapText) {
            this.wrapText = wrapText;
        }

        public String getFontName() {
            return fontName;
        }

        public void setFontName(String fontName) {
            this.fontName = fontName;
        }

        public Short getFontSize() {
            return fontSize;
        }

        public void setFontSize(Short fontSize) {
            this.fontSize = fontSize;
        }

        public Short getVerticalAlign() {
            return verticalAlign;
        }

        public void setVerticalAlign(Short verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

        public Short getAlign() {
            return align;
        }

        public void setAlign(Short align) {
            this.align = align;
        }

        public Short getFillForegroundColor() {
            return fillForegroundColor;
        }

        public void setFillForegroundColor(Short fillForegroundColor) {
            this.fillForegroundColor = fillForegroundColor;
        }

        public Integer getDataType() {
            return dataType;
        }

        public void setDataType(Integer dataType) {
            this.dataType = dataType;
        }

        public String getTitleKey() {
            return titleKey;
        }

        public void setTitleKey(String titleKey) {
            this.titleKey = titleKey;
        }

        public List<Header> getChildren() {
            return children;
        }

        public void setChildren(List<Header> children) {
            this.children = children;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDataProp() {
            return dataProp;
        }

        public void setDataProp(String dataProp) {
            this.dataProp = dataProp;
        }
    }

    /**
     * 输入源 - 表格数据体
     */
    public static class Data {
        /**
         * 数据的字段名（必要）
         */
        private String dataProp;
        /**
         * 数据类型（非必要）
         */
        private Integer dataType;
        /**
         * 单元格值（必要）
         */
        private Object value;
        /**
         * 唯一标识（非必要）
         * 唯一标识为空时不进行合并；
         * 相邻的单元格唯一标识相同，则进行合并；
         */
        private String key;
        /**
         * 是否以此单元格设置冻结单元格（非必要），未设置时，默认冻结表头单元格
         * 值为：Y/N
         */
        private String splitFreeze;
        /**
         * 单元格填充色（非必要）
         */
        private Short fillForegroundColor;
        /**
         * 单元格垂直位置（非必要）
         * 见：HSSFCellStyle.VERTICAL_*
         */
        private Short verticalAlign;
        /**
         * 单元格水平位置（非必要）
         * 见：HSSFCellStyle.ALIGN_*
         */
        private Short align;
        /**
         * 单元格是否支持换行(非必要）
         */
        private Boolean wrapText;
        /**
         * 字体名称（非必要）
         */
        private String fontName;
        /**
         * 字体大小（非必要）
         */
        private Short fontSize;

        public Data() {
            this.fillForegroundColor = null;
        }

        public Data(Object value) {
            this.value = value;
        }

        public Data(Object value, Integer dataType) {
            this.dataType = dataType;
            this.value = value;
        }

        public Boolean getWrapText() {
            return wrapText;
        }

        public Data setWrapText(Boolean wrapText) {
            this.wrapText = wrapText;
            return this;
        }

        public String getFontName() {
            return fontName;
        }

        public Data setFontName(String fontName) {
            this.fontName = fontName;
            return this;
        }

        public Short getFontSize() {
            return fontSize;
        }

        public Data setFontSize(Short fontSize) {
            this.fontSize = fontSize;
            return this;
        }

        public Short getVerticalAlign() {
            return verticalAlign;
        }

        public Data setVerticalAlign(Short verticalAlign) {
            this.verticalAlign = verticalAlign;
            return this;
        }

        public Short getAlign() {
            return align;
        }

        public Data setAlign(Short align) {
            this.align = align;
            return this;
        }

        public Short getFillForegroundColor() {
            return fillForegroundColor;
        }

        public Data setFillForegroundColor(Short fillForegroundColor) {
            this.fillForegroundColor = fillForegroundColor;
            return this;
        }

        public String getSplitFreeze() {
            return splitFreeze;
        }

        public Data setSplitFreeze(String splitFreeze) {
            this.splitFreeze = splitFreeze;
            return this;
        }

        public Integer getDataType() {
            return dataType;
        }

        public Data setDataType(Integer dataType) {
            this.dataType = dataType;
            return this;
        }

        public String getDataProp() {
            return dataProp;
        }

        public Data setDataProp(String dataProp) {
            this.dataProp = dataProp;
            return this;
        }

        public Object getValue() {
            return value;
        }

        public Data setValue(Object value) {
            this.value = value;
            return this;
        }

        public String getKey() {
            return key;
        }

        public Data setKey(String key) {
            this.key = key;
            return this;
        }
    }

    /**
     * 输入源
     */
    public static class SourceData {
        private String sheetName;
        private List<Header> headerList;
        /**
         * 第一版：表格数据体（不支持合并）
         * Map<String, Object>: key: dataProp, value: Integer/String/Double
         */
        private List<Map<String, Object>> dataList;
        /**
         * 第一版：表格数据体（尾部）（不支持合并）
         * Map<String, Object>: key: dataProp, value: Integer/String/Double
         */
        private List<Map<String, Object>> footList;
        /**
         * 第二版：表格数据体（支持合并）
         */
        private List<List<Data>> dataObjList;
        /**
         * 第二版：表格数据体（尾部）（支持合并）
         */
        private List<List<Data>> footObjList;

        public SourceData() {
        }

        public SourceData(String sheetName, List<Header> headerList, List<Map<String, Object>> dataList, List<Map<String, Object>> footList) {
            this.sheetName = sheetName;
            this.headerList = headerList;
            this.dataList = dataList;
            this.footList = footList;
        }

        public List<List<Data>> getDataObjList() {
            if (null == dataObjList || dataObjList.size() == 0) {
                if (null != dataList && dataList.size() > 0) {
                    return trasObjDataListFromMapList(dataList);
                }
            }
            return dataObjList;
        }

        public void setDataObjList(List<List<Data>> dataObjList) {
            this.dataObjList = dataObjList;
        }

        public List<List<Data>> getFootObjList() {
            if (null == footObjList || footObjList.size() == 0) {
                if (null != footList && footList.size() > 0) {
                    return trasObjDataListFromMapList(footList);
                }
            }
            return footObjList;
        }

        public void setFootObjList(List<List<Data>> footObjList) {
            this.footObjList = footObjList;
        }

        public String getSheetName() {
            return sheetName;
        }

        public void setSheetName(String sheetName) {
            this.sheetName = sheetName;
        }

        public List<Map<String, Object>> getFootList() {
            return footList;
        }

        public void setFootList(List<Map<String, Object>> footList) {
            this.footList = footList;
        }

        public List<Header> getHeaderList() {
            return headerList;
        }

        public void setHeaderList(List<Header> headerList) {
            this.headerList = headerList;
        }

        public List<Map<String, Object>> getDataList() {
            return dataList;
        }

        public void setDataList(List<Map<String, Object>> dataList) {
            this.dataList = dataList;
        }

        /**
         * 兼容第一版本
         */
        private List<List<Data>> trasObjDataListFromMapList(List<Map<String, Object>> dataList) {
            if (null == this.headerList || this.headerList.size() == 0) {
                return new ArrayList<>();
            }
            // 字段顺序
            List<String> dataPropList = new ArrayList<>();
            // 值的类型
            Map<String, Integer> dataTypeMap = new HashMap<>();
            recursionColDataProp(dataPropList, dataTypeMap, this.headerList);

            List<List<Data>> rowList = new ArrayList<>();
            for (Map<String, Object> map : dataList) {
                List<Data> cellList = new ArrayList<>();
                for (String dataProp : dataPropList) {
                    Object value = map.get(dataProp);
                    Data data = new Data();
                    data.setDataProp(dataProp);
                    data.setDataType(dataTypeMap.get(dataProp));
                    data.setValue(value);
                    cellList.add(data);
                }
                rowList.add(cellList);
            }
            return rowList;
        }
    }

    /**
     * ExcelUtil导出时格式化行数据
     */
    private static class RowData {
        private Integer rowIndex;
        private List<CellData> cellDataList;

        public RowData() {
        }

        public RowData(Integer rowIndex, List<CellData> cellDataList) {
            this.rowIndex = rowIndex;
            this.cellDataList = cellDataList;
        }

        public Integer getRowIndex() {
            return rowIndex;
        }

        public void setRowIndex(Integer rowIndex) {
            this.rowIndex = rowIndex;
        }

        public List<CellData> getCellDataList() {
            return cellDataList;
        }

        public void setCellDataList(List<CellData> cellDataList) {
            this.cellDataList = cellDataList;
        }
    }

    /**
     * ExcelUtil导出时格式化单元格数据
     */
    private static class CellData {
        /**
         * 单元格的值
         */
        private Object value;
        private Integer dataType;
        /**
         * 开始行
         */
        private Integer startRow;
        /**
         * 结束行
         */
        private Integer endRow;
        /**
         * 开始列
         */
        private Integer startCol;
        /**
         * 结束列
         */
        private Integer endCol;
        /**
         * 数据字段名
         */
        private String dataProp;
        /**
         * 是否以此单元格设置冻结
         */
        private boolean splitFreeze;
        /**
         * 单元格填充色 {@link IndexedColors}
         */
        private Short fillForegroundColor;
        /**
         * 单元格垂直位置（非必要）
         * 见：HSSFCellStyle.VERTICAL_*
         */
        private Short verticalAlign;
        /**
         * 单元格水平位置（非必要）
         * 见：HSSFCellStyle.ALIGN_*
         */
        private Short align;
        /**
         * 单元格是否支持换行(非必要）
         */
        private Boolean wrapText;
        /**
         * 字体名称（非必要）
         */
        private String fontName;
        /**
         * 字体大小（非必要）
         */
        private Short fontSize;

        /**
         * 是否有自定义单元格样式
         *
         * @return Boolean
         */
        public boolean hasCustomCellStyle() {
            return null != fillForegroundColor
                    || null != verticalAlign
                    || null != align
                    || null != wrapText
                    || null != fontName
                    || null != fontSize;
        }

        /**
         * 是否需要合并
         */
        public boolean isNeedMerge() {
            boolean flag = !(Objects.equals(startRow, endRow) && Objects.equals(startCol, endCol));
            return flag;
        }

        public boolean isSplitFreeze() {
            return splitFreeze;
        }

        public Boolean getWrapText() {
            return wrapText;
        }

        public void setWrapText(Boolean wrapText) {
            this.wrapText = wrapText;
        }

        public String getFontName() {
            return fontName;
        }

        public void setFontName(String fontName) {
            this.fontName = fontName;
        }

        public Short getFontSize() {
            return fontSize;
        }

        public void setFontSize(Short fontSize) {
            this.fontSize = fontSize;
        }

        public Short getVerticalAlign() {
            return verticalAlign;
        }

        public void setVerticalAlign(Short verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

        public Short getAlign() {
            return align;
        }

        public void setAlign(Short align) {
            this.align = align;
        }

        public Short getFillForegroundColor() {
            return fillForegroundColor;
        }

        public void setFillForegroundColor(Short fillForegroundColor) {
            this.fillForegroundColor = fillForegroundColor;
        }

        public void setSplitFreeze(boolean splitFreeze) {
            this.splitFreeze = splitFreeze;
        }

        public Integer getDataType() {
            return dataType;
        }

        public void setDataType(Integer dataType) {
            this.dataType = dataType;
        }

        public String getDataProp() {
            return dataProp;
        }

        public void setDataProp(String dataProp) {
            this.dataProp = dataProp;
        }

        public Integer getStartRow() {
            return startRow;
        }

        public Integer getEndRow() {
            return endRow;
        }

        public void setEndRow(Integer endRow) {
            this.endRow = endRow;
        }

        public void setStartCol(Integer startCol) {
            this.startCol = startCol;
        }

        public void setStartRow(Integer startRow) {
            this.startRow = startRow;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public int getStartCol() {
            return startCol;
        }

        public void setStartCol(int startCol) {
            this.startCol = startCol;
        }

        public int getEndCol() {
            return endCol;
        }

        public void setEndCol(Integer endCol) {
            this.endCol = endCol;
        }
    }

    /**
     * 创建单元格样式的参数对象
     */
    private static class StyleParam {
        /**
         * 单元格填充色 {@link IndexedColors}
         */
        private Short fillForegroundColor;
        /**
         * 单元格垂直位置（非必要）
         * 见：HSSFCellStyle.VERTICAL_*
         */
        private Short verticalAlign;
        /**
         * 单元格水平位置（非必要）
         * 见：HSSFCellStyle.ALIGN_*
         */
        private Short align;
        /**
         * 单元格是否支持换行(非必要）
         */
        private Boolean wrapText;
        /**
         * 字体名称（非必要）
         */
        private String fontName;
        /**
         * 字体大小（非必要）
         */
        private Short fontSize;
        /**
         * 字体是否加粗
         */
        private Boolean bold;

        public StyleParam() {
            this.bold = false;
            this.fontName = "宋体";
            this.fontSize = 10;
            this.wrapText = true;
            // this.fillForegroundColor = IndexedColors.GREY_25_PERCENT.getIndex();
            this.fillForegroundColor = IndexedColors.WHITE.getIndex();
            this.verticalAlign = HSSFCellStyle.VERTICAL_CENTER;
            this.align = HSSFCellStyle.ALIGN_CENTER;
        }

        public StyleParam(CellData cellData) {
            this();
            if (null != cellData.getFillForegroundColor()) {
                this.fillForegroundColor = cellData.getFillForegroundColor();
            }
            if (null != cellData.getAlign()) {
                this.align = cellData.getAlign();
            }
            if (null != cellData.getVerticalAlign()) {
                this.verticalAlign = cellData.getVerticalAlign();
            }
            if (null != cellData.getWrapText()) {
                this.wrapText = cellData.getWrapText();
            }
            if (null != cellData.getFontName()) {
                this.fontName = cellData.getFontName();
            }
            if (null != cellData.getFontSize()) {
                this.fontSize = cellData.getFontSize();
            }
        }

        public StyleParam(Short fillForegroundColor) {
            this();
            this.fillForegroundColor = fillForegroundColor;
        }

        public Boolean getBold() {
            return bold;
        }

        public void setBold(Boolean bold) {
            this.bold = bold;
        }

        public String getFontName() {
            return fontName;
        }

        public Short getFontSize() {
            return fontSize;
        }

        public Boolean getWrapText() {
            return wrapText;
        }

        public Short getFillForegroundColor() {
            return fillForegroundColor;
        }

        public Short getVerticalAlign() {
            return verticalAlign;
        }

        public Short getAlign() {
            return align;
        }

        public void setFontName(String fontName) {
            this.fontName = fontName;
        }

        public void setFontSize(Short fontSize) {
            this.fontSize = fontSize;
        }

        public void setWrapText(Boolean wrapText) {
            this.wrapText = wrapText;
        }

        public void setFillForegroundColor(Short fillForegroundColor) {
            this.fillForegroundColor = fillForegroundColor;
        }

        public void setVerticalAlign(Short verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

        public void setAlign(Short align) {
            this.align = align;
        }
    }
}
