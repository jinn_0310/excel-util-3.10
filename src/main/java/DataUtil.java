import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import nc.impl.saas.file.ExcelUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class DataUtil {

    public static <T> List<T> getSourceData(String fileName, Class<T> clz) {
        try {
            String json = readFromClasspath(fileName);
            return JSONArray.parseArray(json, clz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 从 classpath 中获取获取文件内容
     *
     * @param name 文件名
     * @return 文件内容
     * @throws IOException 异常
     */
    public static String readFromClasspath(String name) throws IOException {
        InputStream is = null;
        try {
            is = ExcelUtil.class.getClassLoader().getResourceAsStream(name);

            if (null != is) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {

                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = is.read(bytes)) != -1) {
                        bos.write(bytes, 0, length);
                    }
                    return bos.toString();
                } finally {
                    close(bos);
                }
            }
        } finally {
            close(is);
        }
        return null;
    }

    public static void close(InputStream... inputStreams) {
        if (inputStreams != null) {
            for (InputStream stream : inputStreams) {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void close(OutputStream... outputStreams) {
        if (outputStreams != null) {
            for (OutputStream stream : outputStreams) {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
