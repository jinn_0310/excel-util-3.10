import com.alibaba.fastjson.JSON;
import nc.impl.saas.file.Excel2007Util;
import nc.impl.saas.file.ExcelUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ExcelUtilTest {
    public static void main(String[] args) throws IOException {
        String fileName = "test.json";
        List<Excel2007Util.SourceData> sourceDataList = DataUtil.getSourceData(fileName, Excel2007Util.SourceData.class);
//        List<ExcelUtil.SourceData> sourceDataList = DataUtil.getSourceData(fileName, ExcelUtil.SourceData.class);

        System.out.println(JSON.toJSONString(sourceDataList));

        String path = "/Users/yuanjin/Downloads/test.xls";
//        ExcelUtil.buildExcelFromSourceData(sourceDataList, path);
        Excel2007Util.buildExcelFromSourceData(sourceDataList, path);

    }
}
